#!/bin/python
import pandas as pd
from root_pandas import to_root
import pickle
import numpy as np
import sys

case = sys.argv[1]
bins_str = sys.argv[2]
frt = int(sys.argv[3])
scd = int(sys.argv[4])

df_toyfit = []
#for seed in range(1, 101):
#for seed in range(101, 201):
#for seed in range(201, 301):
#for seed in range(301, 401):
#for seed in range(401, 501):
#for seed in range(501, 601):
#for seed in range(601, 701):
#for seed in range(701, 801):
#for seed in range(801, 901):
#for seed in range(901, 1001):
#for seed in range(1001, 1101):
#for seed in range(1101, 1201):
#for seed in range(1201, 1301):
#for seed in range(1301, 1501):
#for seed in range(1301, 1501):
#for seed in range(1701, 1901):
#for seed in list(range(1501, 1701)) + list(range(1901, 2101)): 
#for seed in list(range(1701, 1901)) + list(range(1301, 1501)): 
#for seed in range(2201, 2301): 
#for seed in range(2301, 2401): 
#for seed in range(2401, 2501): 
#for seed in range(2501, 2601): 
#for seed in range(2601, 2701): #q2cthl,  binned fit test stat
#for seed in range(2701, 2801): #q2cthl,  binned fit test stat
#for seed in range(2801, 2901): #q2cthl,  binned fit test stat
#for seed in range(2901, 3001): #q2cthl,  binned fit test stat
for seed in range(frt, scd): #q2cthl,  unbinned fit test stat
    #if seed == 86 or  seed == 87 or seed == 88 or seed == 89 or seed == 90: continue #case1
    #df_toyfit += [pickle.load(open('/disk/lhcb_data/amathad/diffdensity/results_case3/'+str(seed)+'-'+case+'-'+bins_str+'-res.p', 'rb'))]
    df_toyfit += [pickle.load(open('/disk/lhcb_data/amathad/diffdensity/results/'+str(seed)+'-'+case+'-'+bins_str+'-res.p', 'rb'))]
print(len(df_toyfit))

df = pd.concat(df_toyfit, axis=0, ignore_index=False)
df = df.reset_index(drop=True)
print('df filled', df)
for d_k in list(df.keys()): df[d_k] = pd.to_numeric(df[d_k]) #go to do this before writing to root (correct 'Type' of the object is recognised)
#to_root(df, 'results_case3/toy_result_Mu_'+case+'_'+bins_str+'.root', key='tree')
to_root(df, 'results/toy_result_Mu_'+case+'_'+bins_str+'.root', key='tree')
