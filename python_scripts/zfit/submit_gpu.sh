#!/bin/bash

source ~/job_pool.sh
function runpy {
	last=${@:$#} # last parameter 
	other=${*%${!#}} # all parameters except the last
	python $other > $last 2>&1
	#python $@
}   
# initialize the job pool to allow 2 parallel jobs and echo commands
job_pool_init 8 0

# run jobs
while read -r LINE; do job_pool_run "$LINE"; done < commands_run.txt

# wait until all jobs complete before continuing
job_pool_wait

# don't forget to shut down the job pool
job_pool_shutdown
