###### - arguments
import sys
seed     = int(sys.argv[1])
gpu      = sys.argv[2]
case     = sys.argv[3]
bins_str = sys.argv[4]
ngen     = float(sys.argv[5])
floatcase= sys.argv[6]
print(floatcase)
#####
##### - TF packages
import os
os.environ["CUDA_VISIBLE_DEVICES"] = gpu
import zfit
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
zfit.run.sess = sess #run in an instance of RunManager
zfit.settings.set_seed(seed) #set the seed for reproducibility
#####
###### - model related
from src.common_params import mlepton, Mn
from src.model_4body_partial_binned import MyPdf
from src.util_zfit import fill_df, get_floating, GaussianConstraint, get_FF_mean_cov
from src.util_zfit import BinnedNLL
######
###### - python packages
import time
start = time.time()
from root_pandas import read_root, to_root
import pandas as pd
import numpy as np
import pickle
from math import pi
#####

######
#lepton mass
ml          = mlepton['Mu']
#Wilson coefficients and form factors to fit
params = {}
params['CVL_real']= zfit.Parameter(name = 'CVL_real', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CVR_imag']= zfit.Parameter(name = 'CVR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
params['CVL_imag']= zfit.Parameter(name = 'CVL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSR_imag']= zfit.Parameter(name = 'CSR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSL_imag']= zfit.Parameter(name = 'CSL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CTL_imag']= zfit.Parameter(name = 'CTL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
lower       = ((zfit.run(ml)**2, -1.),)
upper       = ((zfit.run(Mn)**2,  1.),)
limits      = (lower, upper)
obs         = zfit.Space(obs=['q2', case.split('2')[1]], limits=limits)
bins_true   = (int(bins_str), int(bins_str)) #NB: make sure that the response matrix is of appropriate shape
ngenerate   = tf.cast(ngen, tf.float64)
edges       = [tf.cast(tf.linspace(low, up, bt+1), tf.float64) for low,up,bt in zip(lower[0],upper[0], bins_true)]
f = open("../FF_cov/LambdabLambdac_results.dat", "r")
floatcvr = False
floatcsr = False
floatcsl = False
floatctl = False
if floatcase == 'floatcvr':
    floatcvr = True
elif floatcase == 'floatcsr':
    floatcsr = True
elif floatcase == 'floatcsl':
    floatcsl = True
elif floatcase == 'floatctl':
    floatctl = True
elif floatcase == 'floatcslcsr':
    floatcsl = True
    floatcsr = True
elif floatcase == 'floatcslctl':
    floatcsl = True
    floatctl = True
elif floatcase == 'floatall':
    floatcvr = True
    floatcsr = True
    floatcsl = True
    floatctl = True

if 'q2cthl' == case: 
    params['CVR_real']=zfit.Parameter(name='CVR_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcvr)
    params['CSR_real']=zfit.Parameter(name='CSR_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsr) 
    params['CSL_real']=zfit.Parameter(name='CSL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsl) 
    params['CTL_real']=zfit.Parameter(name='CTL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatctl)
    for l in f.readlines(): params[l.split()[0]] = zfit.Parameter(name = l.split()[0], value = float(l.split()[1]), lower_limit = float(l.split()[1]) - 0.5 * abs(float(l.split()[1])), upper_limit = float(l.split()[1]) + 0.5 * abs(float(l.split()[1])), step_size = 0.05, floating = True)
    nfits = 50
elif 'q2cthlcthpphp' == case: 
    lower       = ((zfit.run(ml)**2, -1., -1., 0.),)
    upper       = ((zfit.run(Mn)**2,  1.,  1., 2.*pi),)
    limits      = (lower, upper)
    obs         = zfit.Space(obs=['q2', 'cthl', 'cthp', 'php'], limits=limits)
    bins_true   = (int(bins_str), int(bins_str), int(bins_str), int(bins_str)) #NB: make sure that the response matrix is of appropriate shape
    edges       = [tf.cast(tf.linspace(low, up, bt+1), tf.float64) for low,up,bt in zip(lower[0],upper[0], bins_true)]
    params['CVR_real']=zfit.Parameter(name='CVR_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcvr)
    params['CSR_real']=zfit.Parameter(name='CSR_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsr) 
    params['CSL_real']=zfit.Parameter(name='CSL_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsl) 
    params['CTL_real']=zfit.Parameter(name='CTL_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatctl)
    params['PLB']     =zfit.Parameter(name = 'PLB', value = 0.06, lower_limit = -1., upper_limit = 1., step_size=0.01,  floating = True)
    params['AL']      =zfit.Parameter(name = 'AL',  value = 0.18, lower_limit = -1., upper_limit = 1., step_size=0.05,  floating = True)
    for l in f.readlines(): params[l.split()[0]] = zfit.Parameter(name = l.split()[0], value = float(l.split()[1]), lower_limit = float(l.split()[1]) - 0.5 * abs(float(l.split()[1])), upper_limit = float(l.split()[1]) + 0.5 * abs(float(l.split()[1])), step_size = 0.05, floating = True)
    ngenerate   = 1./5. * 0.0159/0.0628 * ngenerate
    nfits = 20
######
f.close()
print(limits)
print(zfit.run(edges))
print(bins_true)
print(zfit.run(ngenerate))

######
#define true pdf
Pdf_true   = MyPdf(name = 'Mu', params = params, obs = obs, case = case, particleID = 'pos', edges=edges)
#generate sample and normalise
nobs_true  = zfit.run(Pdf_true.gen_pdf_bincntr(nsamples=1, ngen=ngenerate, seed = seed))
ntot_fluc  = np.sum(nobs_true)
nobs_true  = nobs_true/ntot_fluc
#define minimizer
minimizer   = zfit.minimize.MinuitMinimizer()
######

###### - Binned Loss
#get pdf evaluated at the bin centers and reshape to bins
prb_center_true  = Pdf_true.pdf_bincntr()
prb_center_true  = tf.reshape(prb_center_true, bins_true)
nobs_true        = tf.reshape(nobs_true, bins_true)

#Get response matrix
mRecoTrue  = tf.convert_to_tensor(pickle.load( open( "/disk/lhcb_data/amathad/diffdensity/responsematrix/"+case+"-"+bins_str+".p", "rb" )))
sum_str = 'ijkl,kl->ij'
if 'q2cthlcthpphp' == case: sum_str = 'ijkl,klmn->ijmn'
conv = lambda dist: tf.einsum(sum_str, mRecoTrue, dist)
nobs_reco = conv(nobs_true)
prb_reco  = conv(prb_center_true) #no need to normalise as it was already been!

#define loss to minimize with gaussian constraint
NLL  = BinnedNLL(nobs_reco, prb_reco,  tf.cast(ntot_fluc, tf.float64), extended = False, yldinit = None)
#constrain FF
ff_mean, ff_cov, ff_names = get_FF_mean_cov()
ff_params = [params[ff_name] for ff_name in ff_names]
NLL  -= GaussianConstraint(ff_params, ff_mean, ff_cov)  #Ltot = L * Lg => NLLtot = NLL - LogLg
#if case == 'q2cthlcthpphp':
#    #constrain plb and al
#    plal_mean = np.array([0.06, 0.18])
#    plal_cov  = tf.linalg.diag(np.array([0.073, 0.45])**2)
#    plal_params = [params['PLB'], params['AL']]
#    NLL  -= GaussianConstraint(plal_params, plal_mean, plal_cov)  
#make loss func
loss  = zfit.loss.SimpleLoss(lambda : NLL, errordef = 0.5)
#######

######
#define dataframe to store
col  = []
col += ['fval']
col += ['has_accurate_covar']
col += ['has_posdef_covar']
col += ['has_made_posdef_covar']
for p in get_floating(params): 
    col += [p.name]
    col += [p.name+'_err']
######

######
#fit each toy nfits times and store results
df_toyfit = pd.DataFrame(index=range(nfits), columns=col)
for j in range(nfits):
    #randomize the params before fitting (NB: Does not change the values in the sampling)
    floating_params = get_floating(params)
    #floating_params = [p for p in floating_params if ('a1' not in p.name and 'a0' not in p.name and 'PLB' not in p.name and 'AL' not in p.name)]
    floating_params = [p for p in floating_params if ('a1' not in p.name and 'a0' not in p.name)]
    print('Values before randomising parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))
    for param in floating_params: 
        if 'PLB' in param.name:
            param.randomize(minval= 0.2 * zfit.run(param.lower_limit), maxval= 0.2 * zfit.run(param.upper_limit))
        else:
            param.randomize(minval= 0.5 * zfit.run(param.lower_limit), maxval= 0.5 * zfit.run(param.upper_limit))
    print('Values after randomising parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))

    result = minimizer.minimize(loss=loss)
    rinfo = result.info['original']
    if (rinfo['is_valid'] and rinfo['has_valid_parameters'] and rinfo['has_covariance'] and not rinfo['hesse_failed'] and not rinfo['is_above_max_edm'] and not rinfo['has_reached_call_limit']):
        fill_df(df_toyfit, result, j, params)
    else:
        for d_k in list(df_toyfit.keys()): df_toyfit[d_k][j] = np.nan
    tf.get_default_graph().finalize() #need to do this to use the same default graph
    print('Fitted sample about', j, 'times')

df_toyfit = df_toyfit.dropna()
df_toyfit = df_toyfit[df_toyfit['fval'].min() == df_toyfit['fval']]
pickle.dump(df_toyfit, open('/disk/lhcb_data/amathad/diffdensity/results/'+str(seed)+'-'+case+'-'+bins_str+'-res.p', 'wb'))
######

end = time.time()
print('Time taken in min', (end - start)/60.)
