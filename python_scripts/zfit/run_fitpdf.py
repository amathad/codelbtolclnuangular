#!/bin/python
#for seed in range(1, 101):  #q2cthpphp, binned fit (bins=60), all gaussian const
#for seed in range(101,201): #q2cthpphp, unbinned fit, all gaussian const, float cvr
#for seed in range(201,301): #q2cthl,    binned fit (bins=20), ff gaussian const, float cvr and csr, resolution,   ngen = 100000.
#for seed in range(301,401): #q2cthl,    binned fit (bins=20), ff gaussian const, float cvr and csr, no resolution,ngen = 100000.
#for seed in range(401,501): #q2cthl,   unbinned fit, ff gaussian const, float cvr and csr, no resolution, ngen = 100000.
#for seed in range(501,601): #q2cthl,    binned fit (bins=20), ff gaussian const, float cvr and csr, resolution, ngen = 10000. and toy gen at bincenter
#for seed in range(601,701):  #q2cthl,    unbinned fit, fix ff, float all wc, no resolution, ngen = 10000. 
#for seed in range(701,801):  #q2cthl,    unbinned fit, fix ff, float all wc except CSL, no resolution, ngen = 10000. 
#for seed in range(801,901):  #q2cthl,    unbinned fit, gaussian ff, float all wc except CSL, no resolution, ngen = 10000. 
#for seed in range(901, 1001): #q2cthpphp,  unbinned fit, fix ff and nuissance, float all wc, no resolution, ngen = 100000. 
#for seed in range(1001, 1101): #q2cthpphp,  unbinned fit, gaussian ff and nuissance, float all wc, no resolution, ngen = 400000., information criterion CT and CSL
#for seed in range(1101, 1201): #q2cthpphp,  unbinned fit, gaussian ff and nuissance, float all wc except csl, no resolution, ngen = 400000. 
#for seed in range(1201, 1301): #q2cthl,  binned fit, gaussian ff and nuissance, float all wc except csl, resolution, ngen = run1, and toy gen at bincenter, 20 times and 100

#for seed in range(1301, 1501): #q2cthl,  binned fit, gaussian ff and nuissance, float all wc except csl, resolution, ngen = run1, and toy gen at bincenter, all four below is 50 times and 200 toys
#for seed in range(1501, 1701): #q2cthpphp,  binned fit, gaussian ff and nuissance, float all wc except csl, resolution, ngen = run1, and toy gen at bincenter
#for seed in range(1701, 1901): #q2cthl,  binned fit, gaussian ff and nuissance, float all wc except csl, resolution, ngen = run1, and toy gen at bincenter
#for seed in range(1901, 2101): #q2cthpphp,  binned fit, gaussian ff and nuissance, float all wc except csl, resolution, ngen = run1, and toy gen at bincenter

#for seed in range(2101, 2201): #q2cthpphp,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = run1, and toy gen at bincenter
#for seed in range(2201, 2301): #q2cthlcthpphp,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = run1, and toy gen at bincenter
#for seed in range(2257, 2301): #q2cthlcthpphp,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = run1, and toy gen at bincenter

#for seed in range(2301, 2401): #q2cthlcthpphp,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = run1, and toy gen at bincenter
#for seed in range(2401, 2501): #q2cthl,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = same yield as above, and toy gen at bincenter
#for seed in range(2501, 2601): #q2cthl,  binned fit, gaussian ff and nuissance, float all, resolution, ngen = pKpi, and toy gen at bincenter
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 1.59/6.28 * 1644000.0
#            log = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
            
#for seed in range(2601, 2701): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 1644.0
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(2701, 2801): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 16440.0
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(2801, 2901): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 164400.0
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(2901, 3001): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 1644000.0
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)

#for seed in range(3001, 3101): #q2cthl,  unbinned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 1644
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_unbinned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(3101, 3201): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 16440
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_unbinned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(3201, 3301): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 164400
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_unbinned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' '+log)
#
#for seed in range(3301, 3401): #q2cthl,  binned fit test stat
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 1644000
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_unbinned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' '+log)

#for seed in range(4001, 4201): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcvr '+log)

#for seed in range(4201, 4401): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsr '+log)
#
#for seed in range(4401, 4601): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsl '+log)
#
#for seed in range(4601, 4801): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatctl '+log)
#
#for seed in range(4801, 5001): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 0 '+case+' '+bins+' '+str(ngen)+' floatcslcsr '+log)
#
#for seed in range(5001, 5201): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcslctl '+log)
#
#for seed in range(5201, 5301): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcvr '+log)
#for seed in range(5301, 5401): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsr '+log)
#for seed in range(5401, 5501): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsl '+log)
#for seed in range(5501, 5601): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatctl '+log)
#for seed in range(5601, 5701): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcslcsr '+log)
#for seed in range(5701, 5801): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcslctl '+log)
#for seed in range(5801, 5901): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatplb '+log)
#for seed in range(5901, 6101): #q2cthl,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthl']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatall '+log)
#for seed in range(6201, 6301): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatall '+log)
#for seed in range(6301, 6401): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcvr '+log)
#for seed in range(6401, 6501): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsr '+log)
#for seed in range(6501, 6601): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcsl '+log)
#for seed in range(6601, 6701): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatctl '+log)
#for seed in range(6701, 6801): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcslcsr '+log)
#for seed in range(6801, 6901): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatcslctl '+log)
#for seed in range(6901, 7001): #q2cthlcthpphp,  binned fit 
#    for bins in ['20']:
#        for case in ['q2cthlcthpphp']:
#            ngen = 7.47e6
#            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
#            print('runpy fit_pdf_binned.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatnothing '+log)
for seed in range(7001, 7002): #q2cthlcthpphp,  binned fit 
    for bins in ['20']:
        for case in ['q2cthl']:
            ngen = 7.47e6
            log  = 'log/fit_'+str(seed)+'-'+case+'-'+bins+'.log'
            print('runpy profile.py '+str(seed)+' 1 '+case+' '+bins+' '+str(ngen)+' floatnothing '+log)
