from .common_params import *
import zfit
import tensorflow_probability as tfp 
import functools

class MyPdf(zfit.pdf.BasePDF):
    def __init__(self, name, params, obs, case, particleID, edges):
        super().__init__(name=name, params=params, obs=obs)
        self.ml        = mlepton[name]
        self.case      = case
        self.particleID = particleID
        coordmin    = [tf.convert_to_tensor(edge[:-1]) for edge in edges]
        coordmax    = [tf.convert_to_tensor(edge[1: ]) for edge in edges]
        coord       = [tf.convert_to_tensor(edge[:-1]) + (edge[1:] - edge[:-1])*0.5 for edge in edges]
        coordminv   = tf.meshgrid(*coordmin, indexing='ij')
        coordmaxv   = tf.meshgrid(*coordmax, indexing='ij') 
        coordv      = tf.meshgrid(*coord,    indexing='ij') 
        self.cmins  = tf.concat([tf.reshape(cmin, [-1, 1]) for cmin in coordminv], axis=1)
        self.cmaxs  = tf.concat([tf.reshape(cmax, [-1, 1]) for cmax in coordmaxv], axis=1)
        self.cntrs  = tf.concat([tf.reshape(c, [-1, 1]) for c in coordv], axis=1)

    def _unnormalized_pdf(self, data):
        return tf.reshape(tf.map_fn(lambda xi: self.diff_decay(tf.boolean_mask(self.cntrs, tf.reduce_all(tf.logical_and(tf.greater(xi, self.cmins), tf.less_equal(xi, self.cmaxs)), axis=1))), data), [-1])

    def pdf_bincntr(self): return self.diff_decay(self.cntrs)/tf.reduce_sum(self.diff_decay(self.cntrs))

    def gen_pdf_bincntr(self, nsamples, ngen, seed): 
        pois = tfp.distributions.Poisson(rate = ngen * self.diff_decay(self.cntrs)/tf.reduce_sum(self.diff_decay(self.cntrs)))
        return pois.sample(nsamples, seed=seed)[0,:]

    def diff_decay(self, data):
        q2         = data[:,0]
        angle      = data[:,1] 
        angle2     = None
        angle3     = None
        if self.case == 'q2cthpphp': 
            angle2 = data[:,2] 
        elif self.case == 'q2cthlcthpphp': 
            angle2 = data[:,2] 
            angle3 = data[:,3] 
        #define normalisation
        pl      = (q2 - self.ml**2)/2./tf.sqrt(q2)
        plc     = plambda(MLb**2, MLc**2, q2)/2./MLb
        N       = (pl * BF_Lc * plc * GF**2 * Vcb**2)/(2.**12 * pi**5 * MLb**2 * tf.sqrt(q2))
        #flav of Lb - only used in the case of azimuthal angle
        flav = 1.
        if self.particleID == 'neg': flav = -1.
        #Wilson coefficients  - Complex
        vl_re  = self.params['CVL_real'] 
        vr_re  = self.params['CVR_real'] 
        sl_re  = self.params['CSL_real']
        sr_re  = self.params['CSR_real']
        tl_re  = self.params['CTL_real']  
        vl_im  = self.params['CVL_imag'] 
        vr_im  = self.params['CVR_imag'] 
        sl_im  = self.params['CSL_imag']
        sr_im  = self.params['CSR_imag']
        tl_im  = self.params['CTL_imag']  
        vl     = CastComplex(tf.complex(vl_re, vl_im))
        vr     = CastComplex(tf.complex(vr_re, vr_im))
        sl     = CastComplex(tf.complex(sl_re, sl_im))
        sr     = CastComplex(tf.complex(sr_re, sr_im))
        g_TL   = CastComplex(tf.complex(tl_re, tl_im))
        g_1PvlMvr = (1. + vl - vr)
        g_1PvlPvr = (1. + vl + vr)
        g_SLPSR   = (sl + sr)
        g_SLMSR   = (sl - sr)
        #define preliminary - Real
        Q_p = Mp**2 - q2 
        Q_n = Mn**2 - q2 
        sqrtQp = tf.sqrt(Q_p)
        sqrtQn = tf.sqrt(Q_n)
        v = tf.sqrt(1. - self.ml**2/q2)
        fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp = self.GetFormFactors(q2) #Form factors
        #define q2 dependent terms - CastComplex
        aV  = CastComplex((f0 * sqrtQp * Mn)/tf.sqrt(q2))
        bV  = CastComplex((fplus * sqrtQn * Mp)/tf.sqrt(q2))
        cV  = CastComplex(CastFloat(tf.sqrt(2.)) * fperp * sqrtQn)
        aA  = CastComplex((g0 * sqrtQn * Mp)/tf.sqrt(q2))
        bA  = CastComplex((gplus * sqrtQp * Mn)/tf.sqrt(q2))
        cA  = CastComplex(CastFloat(tf.sqrt(2.)) * gperp * sqrtQp)
        aS  = CastComplex((f0 * sqrtQp * Mn)/(mb - mc))
        aP  = CastComplex((g0 * sqrtQn * Mp)/(mb + mc))
        aT  = CastComplex(hplus * sqrtQn)
        bT  = CastComplex((CastFloat(tf.sqrt(2.)) * hperp  * Mp * sqrtQn)/tf.sqrt(q2))
        cT  = CastComplex((CastFloat(tf.sqrt(2.)) * htperp * Mn * sqrtQp)/tf.sqrt(q2))
        dT  = CastComplex(htplus * sqrtQp)
        al  = CastComplex(2. * self.ml * v)
        bl  = CastComplex(2. * tf.sqrt(q2) * v)
        #Define I_i terms - Should be complex
        I1  = (-4.*bl*(bT + cT)*g_TL + al*cA*g_1PvlMvr - al*cV*g_1PvlPvr)
        I2  = ( 4.*bl*(bT - cT)*g_TL + al*cA*g_1PvlMvr + al*cV*g_1PvlPvr)
        I3  = ( 4.*al*(bT - cT)*g_TL + bl*cA*g_1PvlMvr + bl*cV*g_1PvlPvr)
        I4  = (-4.*al*(bT + cT)*g_TL + bl*cA*g_1PvlMvr - bl*cV*g_1PvlPvr)
        I5  = ( 4.*bl*(aT - dT)*g_TL + al*bA*g_1PvlMvr + al*bV*g_1PvlPvr)
        I6  = (-4.*bl*(aT + dT)*g_TL + al*bA*g_1PvlMvr - al*bV*g_1PvlPvr)
        I7  = ( 4.*al*(aT + dT)*g_TL - bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I8  = ( 4.*al*(aT - dT)*g_TL + bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I9  = (-(aP*bl*g_SLMSR) + aS*bl*g_SLPSR + aA*al*g_1PvlMvr + al*aV*g_1PvlPvr)
        I10 = (-(aP*bl*g_SLMSR) - aS*bl*g_SLPSR + aA*al*g_1PvlMvr - al*aV*g_1PvlPvr)
        if self.case == 'q2cthlc': 
            #no alc deped, and when plb = 0 dependence on cthlc is removed
            plb = CastComplex(self.params['PLB'])
            cthlc   = CastComplex(angle)
            Asq = (4.*pi**2*((1. + cthlc*plb)*(I2*tf.conj(I2) + I6*tf.conj(I6) + 3.*I10*tf.conj(I10) + 2.*I3*tf.conj(I3) + 2.*I7*tf.conj(I7)) - \
                            (-1. + cthlc*plb)*(I1*tf.conj(I1) + I5*tf.conj(I5) + 2.*I4*tf.conj(I4) + 2.*I8*tf.conj(I8) + 3.*I9*tf.conj(I9))))/3.;
        elif self.case == 'q2cthl':
            #no dependence on plb or alc
            cthl   = CastComplex(angle)
            Asq  = 2.*pi**2*(I2*tf.conj(I2) + I3*tf.conj(I3) + I4*tf.conj(I4) + (I1 - cthl**2*I1)*tf.conj(I1) + 2.*(I10 + cthl*I6)*tf.conj(I10) + \
                            cthl*(-2.*cthl*(I7*tf.conj(I7) + I8*tf.conj(I8)) - cthl*I2*tf.conj(I2) + (-2. + cthl)*I3*tf.conj(I3) + (2. + cthl)*I4*tf.conj(I4) + \
                            2.*(cthl*I5 + I9)*tf.conj(I5) + 2.*(I10 + cthl*I6)*tf.conj(I6)) + 2.*I7*tf.conj(I7) + 2.*I8*tf.conj(I8) + \
                            2.*(cthl*I5 + I9)*tf.conj(I9));
        elif self.case == 'q2phl':
            plb = CastComplex(self.params['PLB'])
            #no alc deped, and when plb = 0 dependence on phl removed
            phl   = CastComplex(flav*angle)
            Asq   =  (pi*(16.*I1*tf.conj(I1) + 48.*I10*tf.conj(I10) + 16.*I2*tf.conj(I2) + 32.*I3*tf.conj(I3) + 32.*I4*tf.conj(I4) + \
                             16.*I5*tf.conj(I5) + 16.*I6*tf.conj(I6) + 32.*I7*tf.conj(I7) + 32.*I8*tf.conj(I8) + 48.*I9*tf.conj(I9) + \
                             (3.*pi**2*plb*(-(I1*tf.conj(I10)) + I9*tf.conj(I2) + I8*tf.conj(I3) - I4*tf.conj(I7) + tf.exp(2.*1j*phl)*(-(I10*tf.conj(I1)) - \
                             I7*tf.conj(I4) + I3*tf.conj(I8) + I2*tf.conj(I9))))/(CastComplex(tf.sqrt(2.))*tf.exp(1j*phl))))/12.;
        elif self.case == 'q2cthp':
            alc = CastComplex(self.params['AL'])
            #no plb dependence, and when alc = 0 dependence on cthp removed
            cthp   = CastComplex(angle)
            Asq  = (4.*pi**2*(I2*tf.conj(I2) + I5*tf.conj(I5) + I6*tf.conj(I6) + 2.*(I3*tf.conj(I3) + I4*tf.conj(I4) + I7*tf.conj(I7) + I8*tf.conj(I8)) + \
                            (I1 + alc*cthp*I1)*tf.conj(I1) + 3.*(1. + alc*cthp)*I10*tf.conj(I10) + 3.*I9*tf.conj(I9) - \
                            alc*cthp*(I2*tf.conj(I2) + I5*tf.conj(I5) + 2.*I3*tf.conj(I3) - 2.*I4*tf.conj(I4) - I6*tf.conj(I6) - 2.*I7*tf.conj(I7) + \
                            2.*I8*tf.conj(I8) + 3.*I9*tf.conj(I9))))/3.;
        elif self.case == 'q2php':
            plb = CastComplex(self.params['PLB'])
            alc = CastComplex(self.params['AL'])
            #depends on alc and plb and if either one of them is zero then php dependence removed
            php   = CastComplex(flav*angle)
            Asq   =  (pi*(8.*I1*tf.conj(I1) + 24.*I10*tf.conj(I10) + 8.*I2*tf.conj(I2) + 16.*I3*tf.conj(I3) + 16.*I4*tf.conj(I4) + 8.*I5*tf.conj(I5) +\
                             8.*I6*tf.conj(I6) + 16.*I7*tf.conj(I7) + 16.*I8*tf.conj(I8) + 24.*I9*tf.conj(I9) +\
                             (alc*pi**2*plb*(3.*I9*tf.conj(I10) + I5*tf.conj(I6) - 2.*I8*tf.conj(I7) +\
                             tf.exp(2.*1j*php)*(I6*tf.conj(I5) - 2.*I7*tf.conj(I8) + 3.*I10*tf.conj(I9))))/(2.*tf.exp(1j*php))))/6.;
        elif self.case == 'q2cthpphp':
            #depends on alc and plb 
            plb = CastComplex(self.params['PLB'])
            alc = CastComplex(self.params['AL'])
            cthp  = CastComplex(angle)
            php   = CastComplex(flav*angle2)
            Asq   = (pi*(2.*I1*tf.conj(I1) + 2.*alc*cthp*I1*tf.conj(I1) + 6.*I10*tf.conj(I10) + 6.*alc*cthp*I10*tf.conj(I10) + 2.*I2*tf.conj(I2) - \
                    2.*alc*cthp*I2*tf.conj(I2) + 4.*I3*tf.conj(I3) - 4.*alc*cthp*I3*tf.conj(I3) + 4.*I4*tf.conj(I4) + 4.*alc*cthp*I4*tf.conj(I4) + \
                    2.*I5*tf.conj(I5) - 2.*alc*cthp*I5*tf.conj(I5) + 2.*I6*tf.conj(I6) + 2.*alc*cthp*I6*tf.conj(I6) + 4.*I7*tf.conj(I7) + \
                    4.*alc*cthp*I7*tf.conj(I7) + 4.*I8*tf.conj(I8) - 4.*alc*cthp*I8*tf.conj(I8) + 6.*I9*tf.conj(I9) - 6.*alc*cthp*I9*tf.conj(I9) + \
                    (alc*tf.sqrt(1. - cthp**2)*pi*plb*(3.*I9*tf.conj(I10) + I5*tf.conj(I6) - 2.*I8*tf.conj(I7) + \
                    tf.exp(2.*1j*php)*(I6*tf.conj(I5) - 2.*I7*tf.conj(I8) + 3.*I10*tf.conj(I9))))/(2.*tf.exp(1j*php))))/3.;
        elif self.case == 'q2cthlcthpphp':
            plb   = CastComplex(self.params['PLB'])
            alc   = CastComplex(self.params['AL'])
            cthl  = CastComplex(angle)
            cthp  = CastComplex(angle2)
            php   = CastComplex(flav*angle3)
            Asq   = (pi*(-4.*(-1. + cthl**2)*(1. + alc*cthp)*I1*tf.conj(I1) + 8.*I10*tf.conj(I10) + 8.*alc*cthp*I10*tf.conj(I10) + \
                    8.*cthl*I6*tf.conj(I10) + 8.*alc*cthl*cthp*I6*tf.conj(I10) + 4.*I2*tf.conj(I2) - 4.*cthl**2*I2*tf.conj(I2) - \
                    4.*alc*cthp*I2*tf.conj(I2) + 4.*alc*cthl**2*cthp*I2*tf.conj(I2) + 4.*I3*tf.conj(I3) - 8.*cthl*I3*tf.conj(I3) + \
                    4.*cthl**2*I3*tf.conj(I3) - 4.*alc*cthp*I3*tf.conj(I3) + 8.*alc*cthl*cthp*I3*tf.conj(I3) - \
                    4.*alc*cthl**2*cthp*I3*tf.conj(I3) + 4.*(1 + cthl)**2*(1. + alc*cthp)*I4*tf.conj(I4) + 8.*cthl*I10*tf.conj(I6) + \
                    8.*alc*cthl*cthp*I10*tf.conj(I6) + 8.*cthl**2*I6*tf.conj(I6) + 8.*alc*cthl**2*cthp*I6*tf.conj(I6) + 8.*I7*tf.conj(I7) - \
                    8.*cthl**2*I7*tf.conj(I7) + 8.*alc*cthp*I7*tf.conj(I7) - 8.*alc*cthl**2*cthp*I7*tf.conj(I7) - \
                    8.*(-1. + alc*cthp)*(cthl*(cthl*I5 + I9)*tf.conj(I5) + (I8 - cthl**2*I8)*tf.conj(I8) + (cthl*I5 + I9)*tf.conj(I9)) - \
                    (alc*tf.sqrt(1. - cthp**2)*plb*(-2.*pi*(cthl*I5 + I9)*(tf.conj(I10) + cthl*tf.conj(I6)) - 2.*(-1. + cthl**2)*pi*I8*tf.conj(I7) - \
                    2.*tf.exp(2.*1j*php)*pi*((-1. + cthl**2)*I7*tf.conj(I8) + (I10 + cthl*I6)*(cthl*tf.conj(I5) + tf.conj(I9)))))/tf.exp(1j*php)))/8.;
        else:
            print(self.case, 'does not exist')
            exit(1)

        Asq = tf.real(Asq)
        return Tau_Lb_gev * N * Asq

    def GetFormFactors(self, q2):
        #Weinber FF
        fplus = ff(q2, mf_pole(Deltaf['fplus']),  self.params['a0_fplus'], self.params['a1_fplus'])
        fperp = ff(q2, mf_pole(Deltaf['fperp']),  self.params['a0_fperp'], self.params['a1_fperp'])
        f0    = ff(q2, mf_pole(Deltaf['f0']),     self.params['a0_f0']   , self.params['a1_f0']   )
        gplus = ff(q2, mf_pole(Deltaf['gplus']),  self.params['a0_gpp'], self.params['a1_gplus'])
        gperp = ff(q2, mf_pole(Deltaf['gperp']),  self.params['a0_gpp'], self.params['a1_gperp'])
        g0    = ff(q2, mf_pole(Deltaf['g0']),     self.params['a0_g0']   , self.params['a1_g0']   )
        hplus = ff(q2, mf_pole(Deltaf['hplus']),  self.params['a0_hplus'], self.params['a1_hplus'])
        hperp = ff(q2, mf_pole(Deltaf['hperp']),  self.params['a0_hperp'], self.params['a1_hperp'])
        htplus= ff(q2, mf_pole(Deltaf['htplus']), self.params['a0_htildepp'],self.params['a1_htildeplus'])
        htperp= ff(q2, mf_pole(Deltaf['htperp']), self.params['a0_htildepp'],self.params['a1_htildeperp'])
        return (fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp)
