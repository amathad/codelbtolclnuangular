#!/bin/python
import numpy as np
import tensorflow as tf
import zfit
import tensorflow_probability as tfp
import pandas as pd
tfd = tfp.distributions

def Mesh(x): 
    xs = tf.meshgrid(*x, indexing='ij')
    return tf.concat([tf.reshape(xi, [-1, 1]) for xi in xs], axis=1)

def check_symmetric(a, rtol=1e-05, atol=1e-08): return np.allclose(a, a.T, rtol=rtol, atol=atol)

def is_symm_pos_def(A):
    if check_symmetric(A):
        try:
            np.linalg.cholesky(A)
            return True
        except np.linalg.LinAlgError:
            return False
    else:
        return False

def get_floating(params): return [p for p in list(params.values()) if p.floating]

def fill_df(df, result, i, params):
    for d_k in list(df.keys()): 
        for r_k in list(result.info['original'].keys()): 
            if r_k == d_k: 
                if 'fval' in d_k: 
                    df[d_k][i] = float(result.info['original'][d_k])
                else: 
                    df[d_k][i] = int(result.info['original'][d_k])

        for p in list(result.params.keys()): 
            if p.name == d_k: df[d_k][i] = float(result.params[params[d_k]]['value'])

        for p, errors in result.hesse().items(): 
            if p.name + '_err' == d_k:  df[d_k][i] = float(errors['error'])

    return None

def BinData(data, bins, obs):
    lower  = obs.limits[0]; upper  = obs.limits[1]
    ranges = []
    for l, u in zip(lower[0], upper[0]): ranges += [(l, u)]
    nobs, edges = np.histogramdd(data, bins=bins, range=ranges)
    return nobs, edges

bin_ctr     = lambda a: a[:-1] + (a[1:] - a[:-1]) * 0.5 
def BinPDF(Pdf, edges, usecenter = True):
    if usecenter:
        coord       = [bin_ctr(tf.convert_to_tensor(edge)) for edge in edges]
        coordv      = tf.meshgrid(*coord, indexing='ij') 
        shape       = tf.shape(coordv[0])
        pts         = tf.concat([tf.reshape(c, [-1, 1]) for c in coordv], axis=1)
        prb         = Pdf.pdf(pts)
    else:
        coordmin    = [tf.convert_to_tensor(edge[:-1]) for edge in edges]
        coordmax    = [tf.convert_to_tensor(edge[1: ]) for edge in edges]
        coordminv   = tf.meshgrid(*coordmin, indexing='ij') 
        coordmaxv   = tf.meshgrid(*coordmax, indexing='ij') 
        shape       = tf.shape(coordminv[0])
        cmins       = tf.concat([tf.reshape(cmin, [-1, 1]) for cmin in coordminv], axis=1)
        cmaxs       = tf.concat([tf.reshape(cmax, [-1, 1]) for cmax in coordmaxv], axis=1)
        prb         = tf.convert_to_tensor([Pdf.Integral(cmax, cmin, tfd.Uniform(low=cmin, high=cmax).sample(20)) for cmin, cmax in zip(zfit.run(cmins), zfit.run(cmaxs))])

    prb         = tf.reshape(prb, shape)
    return prb

def BinnedNLL(nobs, prb, ntot, extended = False, yldinit = None):
    mutot = ntot
    if extended: mutot=zfit.Parameter(name='mutot',value=yldinit,lower_limit= -2.*yldinit, upper_limit = 2.*yldinit, step_size=10., floating = True)
    nll = mutot - ntot - tf.reduce_sum(ntot * nobs * tf.log(mutot * prb)) 
    return nll

def UnBinnedNLL(prb): return -tf.reduce_sum(tf.log(prb))

def get_FF_mean_cov():
    #get mean
    mean = {}
    for l in open("../FF_cov/LambdabLambdac_results.dat", "r").readlines(): mean[l.split()[0]] = float(l.split()[1])
    names = mean.keys()
    mean_list = [mean[n] for n in names]
    #get cov
    covariance = {}
    for l in names: covariance[l] = {}
    for l in open("../FF_cov/LambdabLambdac_covariance.dat", "r").readlines(): covariance[l.split()[0]][l.split()[1]] = float(l.split()[2])
    cov_list = [[covariance[l1][l2] for l2 in names] for l1 in names]
    #convert to arrays
    mean_list = np.array(mean_list)
    cov_list  = np.array(cov_list)
    #check if symmetric and positive definite
    if not is_symm_pos_def(cov_list): 
        print('Not symmetric positive definite cov matrix, exiting')
        exit(0)
    else:
        print('Cov is symmetric and positive definite')
    return mean_list, cov_list, names

def GaussianConstraint(params, mean, cov): 
    const_dstb = tfd.MultivariateNormalFullCovariance(loc=mean, covariance_matrix=cov)
    return const_dstb.log_prob(params)
