from math import pi
import tensorflow as tf
CastComplex  = lambda x : tf.cast(x, dtype = tf.complex128)
CastFloat    = lambda x : tf.cast(x, dtype = tf.float64)

mlepton = {}
#Global constants express all in GeV
GF             = tf.constant(1.166378e-5              , dtype = tf.float64) #GeV^-2
mb             = tf.constant(4.18                     , dtype = tf.float64) #GeV
mc             = tf.constant(1.275                    , dtype = tf.float64) #GeV
Tau_Lb_gev     = tf.constant(1e9 * 1.47e-12/6.582e-16 , dtype = tf.float64) #GeV^-1, where hcross = 6.582e-16 eV * sec 
BF_Lc          = tf.constant(1.59e-2                  , dtype = tf.float64) #GeV^-1, where hcross = 6.582e-16 eV * sec 
MLb            = tf.constant(5.61960                  , dtype = tf.float64) #GeV
MLc            = tf.constant(2.28646                  , dtype = tf.float64) #GeV
#Vcb            = tf.constant(3.871e-2                 , dtype = tf.float64) #value from straub paper
Vcb            = tf.constant(4.22e-2                 , dtype = tf.float64) #avg of incl and excl
mlepton['Tau'] = tf.constant(1.77686                  , dtype = tf.float64) #GeV
mlepton['Mu']  = tf.constant(105.66e-3                , dtype = tf.float64) #GeV
#derived constants
Mp = MLb + MLc
Mn = MLb - MLc
plambda = lambda a, b, c: tf.sqrt(a**2 + b**2 + c**2 - 2.*(a*b + a*c + b*c))
#For form factor
#define delataf
Deltaf = {}
Deltaf['fplus']  = tf.constant(56e-3         , dtype = tf.float64)#GeV
Deltaf['fperp']  = tf.constant(56e-3         , dtype = tf.float64)#GeV
Deltaf['f0']     = tf.constant(449e-3        , dtype = tf.float64)#GeV
Deltaf['gplus']  = tf.constant(492e-3        , dtype = tf.float64)#GeV
Deltaf['gperp']  = tf.constant(492e-3        , dtype = tf.float64)#GeV
Deltaf['g0']     = tf.constant(0.            , dtype = tf.float64)#GeV
Deltaf['hplus']  = tf.constant(6.332 - 6.276 , dtype = tf.float64)#GeV
Deltaf['hperp']  = tf.constant(6.332 - 6.276 , dtype = tf.float64)#GeV
Deltaf['htplus'] = tf.constant(6.768 - 6.276 , dtype = tf.float64)#GeV
Deltaf['htperp'] = tf.constant(6.768 - 6.276 , dtype = tf.float64)#GeV
#define mf_pole, tf_p, zf, Weinberg form factor functions
t0 = Mn**2
M_Bc = tf.constant(6.276, dtype = tf.float64) #GeV, from straub paper
mf_pole = lambda deltaf: M_Bc + deltaf 
tf_plus = lambda Mf_pole: Mf_pole**2
zf = lambda Q2, Tf_p: (tf.sqrt(Tf_p - Q2) - tf.sqrt(Tf_p - t0))/(tf.sqrt(Tf_p - Q2) + tf.sqrt(Tf_p - t0))
ff = lambda Q2, Mf_pole, A0, A1: 1./(1. - Q2/Mf_pole**2) * (A0 + A1 * zf(Q2, tf_plus(Mf_pole)))

