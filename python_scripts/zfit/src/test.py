import numpy as np
import tensorflow as tf
sess = tf.Session()

def Mesh(x): 
    xs = tf.meshgrid(*x, indexing='ij')
    return tf.concat([tf.reshape(xi, [-1, 1]) for xi in xs], axis=1)

def AmplSq(data): return data[:,0] + data[:,1] 

#edges = [ \
#         tf.cast(tf.linspace(1.,3.,3), tf.float64),  \
#         tf.cast(tf.linspace(4.,6.,3), tf.float64),  \
#         tf.cast(tf.linspace(7.,9.,3), tf.float64) \
#         ];
#data  = tf.convert_to_tensor(tf.concat([ \
#    tf.random.uniform(shape=[size,1], minval=1.,maxval=3.,seed=seed), \
#    tf.random.uniform(shape=[size,1], minval=4.,maxval=6.,seed=seed), \
#    tf.random.uniform(shape=[size,1], minval=7.,maxval=9.,seed=seed) \
#    ], axis=1));
#print(zfit.run(edges))
#print(zfit.run(data))
#exit(0)
#
#coordmin    = [tf.convert_to_tensor(edge[:-1]) for edge in edges]
#coordmax    = [tf.convert_to_tensor(edge[1: ]) for edge in edges]
#coord       = [tf.convert_to_tensor(edge[:-1]) + (edge[1:] - edge[:-1])*0.5 for edge in edges]
#coordminv   = tf.meshgrid(*coordmin, indexing='ij')
#coordmaxv   = tf.meshgrid(*coordmax, indexing='ij') 
#coordv      = tf.meshgrid(*coord,    indexing='ij') 
#cmins       = tf.concat([tf.reshape(cmin, [-1, 1]) for cmin in coordminv], axis=1)
#cmaxs       = tf.concat([tf.reshape(cmax, [-1, 1]) for cmax in coordmaxv], axis=1)
#cntrs       = tf.concat([tf.reshape(c, [-1, 1]) for c in coordv], axis=1)
#
#print(zfit.run(data) )
#print(zfit.run(cmins))
#print(zfit.run(cmaxs))
#print(zfit.run(cntrs))
#prb         = tf.reshape(tf.map_fn(lambda xi:AmplSq(tf.boolean_mask(cntrs,tf.reduce_all(tf.logical_and(tf.greater(xi, cmins), tf.less_equal(xi, cmaxs)), axis=1))), data), [-1])
##print(zfit.run(prb).shape)

size = int(8) #always make the grid 1e6
edges = [\
         tf.cast(tf.linspace(1.,  3., 11), tf.float64),  \
         tf.cast(tf.linspace(4.,  6., 11), tf.float64),  \
         tf.cast(tf.linspace(7.,  8., 11), tf.float64),  \
         tf.cast(tf.linspace(9., 10., 11), tf.float64)
        ];
ndim  = len(edges)

coordmin  = [tf.convert_to_tensor(edge[:-1]) for edge in edges]
coordmax  = [tf.convert_to_tensor(edge[1: ]) for edge in edges]
cmins     = Mesh(coordmin)
cmaxs     = Mesh(coordmax)
cds       = tf.concat([cmins, cmaxs], axis=1)
print(sess.run(cds).shape)
binsample = tf.map_fn(lambda cd: Mesh([tf.linspace(cd[i],cd[ndim+i],int(size)) for i in range(ndim)]), cds);
prb = tf.map_fn(lambda bn: tf.reduce_mean(AmplSq(bn)), binsample);
print(sess.run(prb))
