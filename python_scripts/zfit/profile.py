###### - arguments
import sys
seed     = int(sys.argv[1])
gpu      = sys.argv[2]
case     = sys.argv[3]
bins_str = sys.argv[4]
ngen     = float(sys.argv[5])
floatcase= sys.argv[6]
print(floatcase)
#####
##### - TF packages
import os
os.environ["CUDA_VISIBLE_DEVICES"] = gpu
import zfit
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
zfit.run.sess = sess #run in an instance of RunManager
zfit.settings.set_seed(seed) #set the seed for reproducibility
#####
###### - model related
from src.common_params import mlepton, Mn
from src.model_4body_partial_binned import MyPdf
from src.util_zfit import fill_df, get_floating, GaussianConstraint, get_FF_mean_cov
from src.util_zfit import BinnedNLL
######
###### - python packages
import time
start = time.time()
from root_pandas import read_root, to_root
import pandas as pd
import numpy as np
import pickle
from math import pi
#####

######
#lepton mass
ml          = mlepton['Mu']
#Wilson coefficients and form factors to fit
params = {}
params['CVL_real']= zfit.Parameter(name = 'CVL_real', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CVR_imag']= zfit.Parameter(name = 'CVR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
params['CVL_imag']= zfit.Parameter(name = 'CVL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSR_imag']= zfit.Parameter(name = 'CSR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSL_imag']= zfit.Parameter(name = 'CSL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CTL_imag']= zfit.Parameter(name = 'CTL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
lower       = ((zfit.run(ml)**2, -1.),)
upper       = ((zfit.run(Mn)**2,  1.),)
limits      = (lower, upper)
obs         = zfit.Space(obs=['q2', case.split('2')[1]], limits=limits)
bins_true   = (int(bins_str), int(bins_str)) #NB: make sure that the response matrix is of appropriate shape
ngenerate   = tf.cast(ngen, tf.float64)
edges       = [tf.cast(tf.linspace(low, up, bt+1), tf.float64) for low,up,bt in zip(lower[0],upper[0], bins_true)]
f = open("../FF_cov/LambdabLambdac_results.dat", "r")
floatcvr = False
floatcsr = False
floatcsl = False
floatctl = False
if floatcase == 'floatcvr':
    floatcvr = True
elif floatcase == 'floatcsr':
    floatcsr = True
elif floatcase == 'floatcsl':
    floatcsl = True
elif floatcase == 'floatctl':
    floatctl = True
elif floatcase == 'floatcslcsr':
    floatcsl = True
    floatcsr = True
    xmin = -0.5; xmax = 0.5 #csr
    ymin = -0.55; ymax = 0.55 #csl
    pntsize = 100
elif floatcase == 'floatcslctl':
    floatcsl = True
    floatctl = True
    xmin = -0.58; xmax = 0.58 #csl
    ymin = -0.18; ymax = 0.18 #ctl
    pntsize = 100
elif floatcase == 'floatall':
    floatcvr = True
    floatcsr = True
    floatcsl = True
    floatctl = True

if 'q2cthl' == case: 
    params['CVR_real']=zfit.Parameter(name='CVR_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcvr)
    params['CSR_real']=zfit.Parameter(name='CSR_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsr) 
    params['CSL_real']=zfit.Parameter(name='CSL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsl) 
    params['CTL_real']=zfit.Parameter(name='CTL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatctl)
    for l in f.readlines(): params[l.split()[0]] = zfit.Parameter(name = l.split()[0], value = float(l.split()[1]), lower_limit = float(l.split()[1]) - 0.5 * abs(float(l.split()[1])), upper_limit = float(l.split()[1]) + 0.5 * abs(float(l.split()[1])), step_size = 0.05, floating = True)
    nfits = 50
elif 'q2cthlcthpphp' == case: 
    lower       = ((zfit.run(ml)**2, -1., -1., 0.),)
    upper       = ((zfit.run(Mn)**2,  1.,  1., 2.*pi),)
    limits      = (lower, upper)
    obs         = zfit.Space(obs=['q2', 'cthl', 'cthp', 'php'], limits=limits)
    bins_true   = (int(bins_str), int(bins_str), int(bins_str), int(bins_str)) #NB: make sure that the response matrix is of appropriate shape
    edges       = [tf.cast(tf.linspace(low, up, bt+1), tf.float64) for low,up,bt in zip(lower[0],upper[0], bins_true)]
    params['CVR_real']=zfit.Parameter(name='CVR_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcvr)
    params['CSR_real']=zfit.Parameter(name='CSR_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsr) 
    params['CSL_real']=zfit.Parameter(name='CSL_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatcsl) 
    params['CTL_real']=zfit.Parameter(name='CTL_real',value=0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = floatctl)
    params['PLB']     =zfit.Parameter(name = 'PLB', value = 0.06, lower_limit = -1., upper_limit = 1., step_size=0.01,  floating = True)
    params['AL']      =zfit.Parameter(name = 'AL',  value = 0.18, lower_limit = -1., upper_limit = 1., step_size=0.05,  floating = True)
    for l in f.readlines(): params[l.split()[0]] = zfit.Parameter(name = l.split()[0], value = float(l.split()[1]), lower_limit = float(l.split()[1]) - 0.5 * abs(float(l.split()[1])), upper_limit = float(l.split()[1]) + 0.5 * abs(float(l.split()[1])), step_size = 0.05, floating = True)
    ngenerate   = 1./5. * 0.0159/0.0628 * ngenerate
    nfits = 20
######
f.close()
print(limits)
print(zfit.run(edges))
print(bins_true)
print(zfit.run(ngenerate))

######
#define true pdf
Pdf_true   = MyPdf(name = 'Mu', params = params, obs = obs, case = case, particleID = 'pos', edges=edges)
#generate sample and normalise
nobs_true  = zfit.run(Pdf_true.gen_pdf_bincntr(nsamples=1, ngen=ngenerate, seed = seed))
ntot_fluc  = np.sum(nobs_true)
nobs_true  = nobs_true/ntot_fluc
#define minimizer
minimizer   = zfit.minimize.MinuitMinimizer()
######

###### - Binned Loss
#get pdf evaluated at the bin centers and reshape to bins
prb_center_true  = Pdf_true.pdf_bincntr()
prb_center_true  = tf.reshape(prb_center_true, bins_true)
nobs_true        = tf.reshape(nobs_true, bins_true)

#Get response matrix
mRecoTrue  = tf.convert_to_tensor(pickle.load( open( "/disk/lhcb_data2/amathad/diffdensity/responsematrix/"+case+"-"+bins_str+".p", "rb" )))
sum_str = 'ijkl,kl->ij'
if 'q2cthlcthpphp' == case: sum_str = 'ijkl,klmn->ijmn'
conv = lambda dist: tf.einsum(sum_str, mRecoTrue, dist)
nobs_reco = conv(nobs_true)
prb_reco  = conv(prb_center_true) #no need to normalise as it was already been!

#define loss to minimize with gaussian constraint
NLL  = BinnedNLL(nobs_reco, prb_reco,  tf.cast(ntot_fluc, tf.float64), extended = False, yldinit = None)
#constrain FF
ff_mean, ff_cov, ff_names = get_FF_mean_cov()
ff_params = [params[ff_name] for ff_name in ff_names]
NLL  -= GaussianConstraint(ff_params, ff_mean, ff_cov)  #Ltot = L * Lg => NLLtot = NLL - LogLg
#if case == 'q2cthlcthpphp':
#    #constrain plb and al
#    plal_mean = np.array([0.06, 0.18])
#    plal_cov  = tf.linalg.diag(np.array([0.073, 0.45])**2)
#    plal_params = [params['PLB'], params['AL']]
#    NLL  -= GaussianConstraint(plal_params, plal_mean, plal_cov)  
#make loss func
loss  = zfit.loss.SimpleLoss(lambda : NLL, errordef = 0.5)
#######

w1x = np.linspace(xmin, xmax, pntsize)
w2y = np.linspace(ymin, ymax, pntsize)
W1xv, W2yv = np.meshgrid(w1x,w2y)
print(W1xv.shape)
pts = np.concatenate([W1xv.reshape(-1,1), W2yv.reshape(-1,1)], axis=1)
print(pts.shape)
floating_params = get_floating(params)
lls = []
for pt in pts:
    print('Values before setting parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))
    for param in floating_params: 
        if floatcase == 'floatcslctl':
            if 'CSL' in param.name:
                param.set_value(pt[0]); param.floating = False
            elif 'CTL' in param.name:
                param.set_value(pt[1]); param.floating = False
        elif floatcase == 'floatcslcsr':
            if 'CSR' in param.name:
                param.set_value(pt[0]); param.floating = False
            elif 'CSL' in param.name:
                param.set_value(pt[1]); param.floating = False
    print('Values after setting parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))
    result = minimizer.minimize(loss=loss)
    rinfo = result.info['original']
    print('Values after fitting parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))
    lls += [-zfit.run(NLL)]

for param in floating_params: 
    if floatcase == 'floatcslctl':
        if 'CSL' in param.name:
            param.set_value(0.); param.floating = True
        elif 'CTL' in param.name:
            param.set_value(0.); param.floating = True
    elif floatcase == 'floatcslcsr':
        if 'CSR' in param.name:
            param.set_value(0.); param.floating = True
        elif 'CSL' in param.name:
            param.set_value(0.); param.floating = True
print('Values after floating parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))

lltrue = None
convergedtrue = True
while convergedtrue:
    resulttrue = minimizer.minimize(loss=loss)
    rinfotrue  = resulttrue.info['original']
    condtrue   = (rinfotrue['is_valid'] and rinfotrue['has_valid_parameters'] and rinfotrue['has_covariance'] and not rinfotrue['hesse_failed'] and not rinfotrue['is_above_max_edm'] and not rinfotrue['has_reached_call_limit'])
    if condtrue: 
        lltrue = -zfit.run(NLL)
        convergedtrue = False
        print('Values after floating parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))

signf = np.sqrt(-2.*(np.array(lls) - np.array(lltrue)))
#print(signf)
pickle.dump((w1x,w2y,signf), open('/disk/lhcb_data2/amathad/diffdensity/results/signf_'+str(seed)+'-'+case+'-'+bins_str+'-res.p', 'wb'))
print('/disk/lhcb_data2/amathad/diffdensity/results/signf_'+str(seed)+'-'+case+'-'+bins_str+'-res.p')
#######
