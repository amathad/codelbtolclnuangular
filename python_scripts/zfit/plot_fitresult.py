#!/bin/python
import sys
import seaborn as sns
#import matplotlib as mpl
#mpl.use('agg')
#mpl.rcParams.update({'font.size': 20})
import matplotlib.pyplot as plt
#plt.rcParams.update({'figure.max_open_warning': 0})
plt.rcParams['axes.unicode_minus'] = False
from root_pandas import read_root
import numpy as np
from matplotlib.patches import Ellipse
#sns.set(style="white")
sns.set(font_scale=0.9) 
sns.set(style="ticks", color_codes=True)

def plot_cov_ellipse(cov, pos, nstd=2, ax=None, **kwargs):
    def eigsorted(cov):
        vals, vecs = np.linalg.eigh(cov)
        order = vals.argsort()[::-1]
        return vals[order], vecs[:,order]
    if ax is None:ax = plt.gca()
    vals, vecs = eigsorted(cov)
    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    # Width and height are "full" widths, not radius
    width, height = 2 * nstd * np.sqrt(vals)
    ellip = Ellipse(xy=pos, width=width, height=height, angle=theta, **kwargs)
    ax.add_artist(ellip)
    return ellip

def plot_point_cov(points, nstd=2., ax=None, **kwargs):
    pos = points.mean(axis=0)
    cov = np.cov(points, rowvar=False)
    print('Mean', pos)
    print('Cov',  cov)
    return plot_cov_ellipse(cov, pos, nstd, ax, **kwargs)

label = {}
label['CVR_real']      = r'$\mathcal{Re} (C_{V_{R}})$'
label['CVL_real']      = r'$\mathcal{Re} (C_{V_{L}})$'
label['CSR_real']      = r'$\mathcal{Re} (C_{S_{R}})$'
label['CSL_real']      = r'$\mathcal{Re} (C_{S_{L}})$'
label['CTL_real']       = r'$\mathcal{Re} (C_{T})$'
label['a0_fplus'] = r'$a_0(f^+)$'
label['a1_fplus'] = r'$a_1(f^+)$'
label['a0_f0']    = r'$a_0(f^0)$'   
label['a1_f0']    = r'$a_1(f^0)$'   
label['a0_fperp'] = r'$a_0(f^\bot)$'
label['a1_fperp'] = r'$a_1(f^\bot)$'
label['a0_gpp'] = r'$a_0(g^{+,\bot})$'
label['a1_gplus'] = r'$a_1(g^+)$'
label['a0_g0']    = r'$a_0(g^0)$'   
label['a1_g0']    = r'$a_1(g^0)$'   
label['a1_gperp'] = r'$a_1(g^\bot)$'
label['a0_hplus'] = r'$a_0(h^+)$' 
label['a0_hperp'] = r'$a_0(h^\bot)$' 
label['a0_htildepp'] = r'$a_0(\tilde{h}^{+,\bot})$'
label['a1_hplus'] = r'$a_1(h^+)$' 
label['a1_hperp'] = r'$a_1(h^\bot)$' 
label['a1_htildeperp'] = r'$a_1(\tilde{h}^{+})$'
label['a1_htildeplus'] = r'$a_1(\tilde{h}^{\bot})$'
label['PLB'] = r'$P_{\Lambda_b}$'
label['AL']  = r'$A_{\Lambda_c}$'

Trueval = {}
Trueval['CVR_real'] = 0.
Trueval['CSR_real'] = 0.
Trueval['CSL_real'] = 0.
Trueval['CTL_real']  = 0.
Trueval['a0_fplus'] =  0.81458549275845
Trueval['a1_fplus'] =  -4.8987680713973
Trueval['a0_f0'] =  0.74392566003686
Trueval['a1_f0'] =  -4.6476511382467
Trueval['a0_fperp'] =  1.077996185173
Trueval['a1_fperp'] =  -6.4170836206134
Trueval['a0_gpp'] =  0.6846557018386
Trueval['a1_gplus'] =  -4.4311552783827
Trueval['a0_g0'] =  0.73960499809758
Trueval['a1_g0'] =  -4.3664554520299
Trueval['a1_gperp'] =  -4.4633624514401
Trueval['a0_hplus'] =  0.97518189850197
Trueval['a1_hplus'] =  -5.4999842229709
Trueval['a0_hperp'] =  0.70539533108116
Trueval['a1_hperp'] =  -4.3577976693726
Trueval['a0_htildepp'] =  0.67275080357035
Trueval['a1_htildeplus'] =  -4.4321757873186
Trueval['a1_htildeperp'] =  -4.4927847697461
Trueval['PLB'] =  0.06
Trueval['AL']  =  0.18


case = sys.argv[1]
bins_str = sys.argv[2]

#df   = read_root('results_case3/toy_result_Mu_'+case+'_'+bins_str+'.root', key='tree')
df   = read_root('results/toy_result_Mu_'+case+'_'+bins_str+'.root', key='tree')
#cond =  (df['CTL_real'] > -0.4) & (df['CTL_real'] < 0.4) 

#cond =  (df['CVR_real'] > -1.)
#cond =  (df['CVR_real'] > -1.) & (df['has_posdef_covar'] == 1.)
#cond =  (df['AL'] > -0.2) 
#cond =  (df['CSR_real'] > -0.2)
#cond =  (df['CSL_real'] < 1.5)
#cond =  (df['CSL_real'] > -1.5)
#cond =  (df['CVR_real'] > -0.5)
#cond =  (df['CVR_real'] < 1.) & (df['CVR_real'] > -1.)  & \
        #(df['CTL_real'] < 0.5) & (df['CTL_real'] > -0.5)
#cond =  \
        #(df['CSR_real'] < 1.) & (df['CSR_real'] > -1.) &  \
        #(df['CVR_real'] < 1.) & (df['CVR_real'] > -1.) &  \
        #(df['CTL_real'] < 0.5) & (df['CTL_real'] > -0.5) & \
        #(df['CSL_real'] < 1.) & (df['CSL_real'] > -1.)
#df = df[cond]
#df  = df.iloc[:247,:]

#import pickle
#pickle.dump( data, open( "results/data.p", "wb" ) )

print(df.shape)
i = []
for k in list(df.keys()):
    #if ('C' in k or 'a0' in k or 'a1' in k or 'PLB' in k or 'AL' in k) and ('err' not in k): i+=[k]
    if ('C' in k or 'PLB' in k or 'AL' in k) and ('_err' not in k): i+=[k]
print(i)
print(df.shape)
bins_1d = 40
color = 'darkgreen'; color_trueval = 'r'; color_mean = 'navy'
fig, ax = plt.subplots()
#sns_plot = sns.PairGrid(df, vars = i, palette=sns.color_palette("muted"))
sns_plot = sns.PairGrid(df, vars = i, palette=sns.color_palette("muted"))
sns_plot.map_lower(plt.scatter, s=8, alpha = 1.0, color = color)
#sns_plot.map_lower(sns.kdeplot, cmap = 'Greens', shade=True, shade_lowest=False)
#sns_plot.map_upper(sns.kdeplot, color = color)
sns_plot.map_diag(sns.distplot, bins = bins_1d,  
                    hist=True, kde=False, rug=False,
                    hist_kws={"alpha": 0.4, "color": color, "linewidth": 0.7},
                    rug_kws={"alpha": 0.7,  "color": color, "linewidth": 0.7},
                    kde_kws={"alpha": 1.0, "color": color, "linewidth": 0.7},
                 )
for j in range(len(i)):
    for k in range(len(i)):
        xlabel = sns_plot.axes[j,k].get_xlabel()
        ylabel = sns_plot.axes[j,k].get_ylabel()
        if xlabel in list(label.keys()): sns_plot.axes[j,k].set_xlabel(label[xlabel])
        if ylabel in list(label.keys()): sns_plot.axes[j,k].set_ylabel(label[ylabel])
        if j == k:
            mu, sigma = df[i[j]].mean(),  df[i[j]].std()
            cont, _ = np.histogram(df[i[j]].values, bins=bins_1d)
            #cont = cont/cont.sum()
            cont_max = np.max(cont)
            sns_plot.axes[j,j].axvspan(mu-sigma, mu+sigma, ymax = cont_max, alpha=0.20, color=color_mean)
            sns_plot.axes[j,j].axvline(Trueval[i[j]], linestyle='-', ymax = cont_max, linewidth=1.3, color = color_trueval)
            sns_plot.axes[j,j].annotate('$\sigma$ = {0:.2g}'.format(sigma),fontsize = 12, color = color_mean, xy=(0.59, 0.85), xycoords='axes fraction')
            print('Sigma', sigma)
        elif j != k and k < j:
            points = df[[i[k], i[j]]].values
            print(points.shape)
            plot_point_cov(points, nstd=1, alpha=0.35, color=color_mean, ax = sns_plot.axes[j,k])
            #plot_point_cov(points, nstd=2, alpha=0.15, color=color_mean, ax = sns_plot.axes[j,k])
            sns_plot.axes[j,k].annotate(r'$\rho = ${0:.2g}'.format(np.corrcoef(points.T)[0,1]), color = color_mean, fontsize = 12, xy=(0.57, 0.85), xycoords='axes fraction')
            sns_plot.axes[j,k].scatter(Trueval[i[k]], Trueval[i[j]], s=48 , c=color_trueval)

plt.tight_layout()
#plt.savefig('results/results-Mu_'+case+'_'+bins_str+'.pdf')
plt.savefig('results/results-Mu_'+case+'_'+bins_str+'.png')
