###### - arguments
import sys
seed     = int(sys.argv[1])
gpu      = sys.argv[2]
case     = sys.argv[3]
bins_str = sys.argv[4]
ngen     = int(sys.argv[5])
#####
##### - TF packages
import os
os.environ["CUDA_VISIBLE_DEVICES"] = gpu
import zfit
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
zfit.run.sess = sess #run in an instance of RunManager
zfit.settings.set_seed(seed) #set the seed for reproducibility
#####
###### - model related
from src.common_params import mlepton, Mn
from src.model_4body_partial import MyPdf
from src.util_zfit import fill_df, get_floating, GaussianConstraint, get_FF_mean_cov, Mesh, BinData
from src.util_zfit import UnBinnedNLL
######
###### - python packages
import time
start = time.time()
import pandas as pd
import numpy as np
import pickle
from math import pi
#####

######
#lepton mass
ml          = mlepton['Mu']
#Wilson coefficients and form factors to fit
params = {}
params['CVL_real']= zfit.Parameter(name = 'CVL_real', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CVR_imag']= zfit.Parameter(name = 'CVR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
params['CVL_imag']= zfit.Parameter(name = 'CVL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSR_imag']= zfit.Parameter(name = 'CSR_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSL_imag']= zfit.Parameter(name = 'CSL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CTL_imag']= zfit.Parameter(name = 'CTL_imag', value = 0., lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
lower       = ((zfit.run(ml)**2, -1.),)
upper       = ((zfit.run(Mn)**2,  1.),)
limits      = (lower, upper)
obs         = zfit.Space(obs=['q2', case.split('2')[1]], limits=limits)
bins_true   = (int(bins_str), int(bins_str)) #NB: make sure that the response matrix is of appropriate shape
ngenerate   = ngen
f = open("../FF_cov/LambdabLambdac_results.dat", "r")
params['CVR_real']=zfit.Parameter(name='CVR_real',value = 0.5,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = True)
params['CSR_real']=zfit.Parameter(name='CSR_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CSL_real']=zfit.Parameter(name='CSL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False) 
params['CTL_real']=zfit.Parameter(name='CTL_real',value = 0.,lower_limit = -2., upper_limit = 2., step_size=0.05, floating = False)
for l in f.readlines(): params[l.split()[0]] = zfit.Parameter(name = l.split()[0], value = float(l.split()[1]), lower_limit = float(l.split()[1]) - 0.5 * abs(float(l.split()[1])), upper_limit = float(l.split()[1]) + 0.5 * abs(float(l.split()[1])), step_size = 0.05, floating = False)
f.close()
print(limits)
print(ngenerate)

######
#define true pdf
Pdf_true    = MyPdf(name = 'Mu', params = params, obs = obs, case = case, particleID = 'pos')
#define sampling fixing the params to true values and not varying them
sampler     = Pdf_true.create_sampler(n=ngenerate,  fixed_params=get_floating(params), limits=obs) 
#define minimizer
minimizer   = zfit.minimize.MinuitMinimizer()
######

#######- Binned NLL
sampler.resample() 
sampl = zfit.run(sampler)
print(sampl)
#get true obs and pdf values
edges     = [tf.cast(tf.linspace(low, up, bt+1), tf.float64) for low,up,bt in zip(lower[0],upper[0], bins_true)]
ndim      = len(edges)
coordmin  = [edge[:-1] for edge in edges]
coordmax  = [edge[1: ] for edge in edges]
coord     = [edge[:-1] + (edge[1:] - edge[:-1])*0.5 for edge in edges]
cmins     = Mesh(coordmin)
cmaxs     = Mesh(coordmax)
cntrs     = Mesh(coord)
cds       = zfit.run(tf.concat([cmins, cmaxs], axis=1))
#Get norm for integral
normsample  = 2000000
norm_edges  = [tf.cast(tf.linspace(low, up, int(normsample**(1/ndim))), tf.float64) for low,up in zip(lower[0],upper[0])]
norm_cds    = Mesh(norm_edges)
P           = tf.reduce_sum(Pdf_true._unnormalized_pdf(norm_cds))
#get nj and pj
nj_t      = zfit.run(tf.reshape(tf.map_fn(lambda cd: tf.cast(tf.shape(tf.boolean_mask(sampl,tf.reduce_all(tf.logical_and(tf.greater(sampl,cd[:ndim]),tf.less_equal(sampl, cd[ndim:])), axis=1)))[0], tf.float64), cds), bins_true))
print(nj_t)
#pj_t = []
#for cd in cds: pj_t += [Pdf_true._unnormalized_pdf(sampl[np.all(np.logical_and(np.greater(sampl,cd[:ndim]),np.less_equal(sampl,cd[ndim:])),axis=1)])[0]]
#pj_t = tf.reshape(tf.convert_to_tensor(pj_t), bins_true)
#print(zfit.run(pj_t).shape)
pj_t      = tf.reshape(tf.map_fn(lambda cd: tf.reduce_sum(Pdf_true._unnormalized_pdf(tf.boolean_mask(sampl,tf.reduce_all(tf.logical_and(tf.greater(sampl,cd[:ndim]),tf.less_equal(sampl, cd[ndim:])), axis=1)))), cds, parallel_iterations=100), bins_true)
#pj_t      = tf.reshape(Pdf_true._unnormalized_pdf(cntrs), bins_true)
#pj_t      = pj_t/tf.reduce_sum(pj_t)
##Get response matrix
#mRecoTrue   = tf.convert_to_tensor(pickle.load( open( "/disk/lhcb_data/amathad/diffdensity/responsematrix/"+case+"-"+bins_str+".p", "rb" )))
#sum_str     = 'ijkl,kl->ij'
#nj_r        = tf.einsum(sum_str, mRecoTrue, nj_t)
#pj_r        = tf.einsum(sum_str, mRecoTrue, pj_t)
print('here')
nj_r        = nj_t
pj_r        = pj_t
#build NLL
NLL         = -tf.reduce_sum(nj_r * tf.log(pj_r)) + tf.reduce_sum(nj_r) * tf.log(P) 
#print('here')
#ntot        = tf.reduce_sum(nj_r)
#muj_r       = ntot * pj_r
#NLL         = tf.reduce_sum(muj_r - nj_r + nj_r * tf.log(nj_r/muj_r))
#NLL         = -tf.reduce_sum(nj_r * tf.log(nj_r/muj_r))
##constrain FF
#ff_mean, ff_cov, ff_names = get_FF_mean_cov()
#ff_params = [params[ff_name] for ff_name in ff_names]
#NLL  -= GaussianConstraint(ff_params, ff_mean, ff_cov)  #Ltot = L * Lg => NLLtot = NLL - LogLg
#if case == 'q2cthlcthpphp':
#    #constrain plb and al
#    plal_mean = np.array([0.06, 0.18])
#    plal_cov  = tf.linalg.diag(np.array([0.073, 0.45])**2)
#    plal_params = [params['PLB'], params['AL']]
#    NLL  -= GaussianConstraint(plal_params, plal_mean, plal_cov)  
#make loss func
loss  = zfit.loss.SimpleLoss(lambda : NLL, errordef = 0.5)
#######

print('here')
######
#define dataframe to store
col  = []
col += ['fval']
col += ['has_accurate_covar']
col += ['has_posdef_covar']
col += ['has_made_posdef_covar']
for p in get_floating(params): 
    col += [p.name]
    col += [p.name+'_err']
######

######
#fit each toy nfits times and store results
nfits = 20
df_toyfit = pd.DataFrame(index=range(nfits), columns=col)
for j in range(nfits):
    #randomize the params before fitting (NB: Does not change the values in the sampling)
    floating_params = get_floating(params)
    floating_params = [p for p in floating_params if ('a1' not in p.name and 'a0' not in p.name and 'PLB' not in p.name and 'AL' not in p.name)]
    print('Values before randomising parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))
    for param in floating_params: param.randomize(minval= 0.5 * zfit.run(param.lower_limit), maxval= 0.5 * zfit.run(param.upper_limit))
    print('Values after randomising parameters', zfit.run(floating_params), 'Nll', zfit.run(NLL))

    result = minimizer.minimize(loss=loss)
    rinfo  = result.info['original']
    if (rinfo['is_valid'] and rinfo['has_valid_parameters'] and rinfo['has_covariance'] and not rinfo['hesse_failed'] and not rinfo['is_above_max_edm'] and not rinfo['has_reached_call_limit']):
        fill_df(df_toyfit, result, j, params)
    else:
        for d_k in list(df_toyfit.keys()): df_toyfit[d_k][j] = np.nan
    tf.get_default_graph().finalize() #need to do this to use the same default graph
    print('Fitted sample about', j, 'times')

df_toyfit = df_toyfit.dropna()
df_toyfit = df_toyfit[df_toyfit['fval'].min() == df_toyfit['fval']]
pickle.dump(df_toyfit, open('/disk/lhcb_data/amathad/diffdensity/results/'+str(seed)+'-'+case+'-'+bins_str+'-res.p', 'wb'))
######

end = time.time()
print('Time taken in min', (end - start)/60.)
