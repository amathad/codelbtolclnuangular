#!/bin/python

import sys, os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import numpy as np
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
from src.model_4body_partial_binned import *
from src.util_tfa import EstimateMaximum, UnfilteredSample, RunToyMC, Integral, write_root

NPmodel     = sys.argv[1] #Tau, Mu
case        = sys.argv[2]
print(NPmodel, case)
seed = 1

ml   = mlepton[NPmodel]
sess = tf.Session(config = config)
#Wilson coefficients
params = {}
params['CVR_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CVL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CSR_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CSL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CTL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CVR_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CVL_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CSR_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CSL_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CTL_imag'] = tf.constant(0.0 , dtype = tf.float64)
ndim = 2
if 'q2cthlc' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
    params['PLB']      = tf.constant(0.06 , dtype = tf.float64)
elif 'q2cthl' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
elif 'q2cthp' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
    params['AL']      = tf.constant(0.18 , dtype = tf.float64)
elif 'q2phl' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (0., 2.*pi))
    params['PLB']      = tf.constant(0.06 , dtype = tf.float64)
elif 'q2php' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (0., 2.*pi))
    params['PLB']      = tf.constant(0.06 , dtype = tf.float64)
    params['AL']       = tf.constant(0.18 , dtype = tf.float64)
elif 'q2cthpphp' == case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.), (0., 2.*pi))
    params['PLB']      = tf.constant(0.06 , dtype = tf.float64)
    params['AL']       = tf.constant(0.18 , dtype = tf.float64)
    ndim = 3

bins = 20
edges       = [tf.cast(tf.linspace(b[0], b[1], bins+1), tf.float64) for b in bounds]
print(bounds)
print(sess.run(edges))

f = open("../FF_cov/LambdabLambdac_results.dat", "r")
for l in f.readlines(): params[l.split()[0]] = tf.constant(float(l.split()[1]), dtype = tf.float64)
f.close()

data_ph = tf.placeholder(tf.float64, shape = (None, ndim))
pdf     = MyPdf(name=NPmodel, params=params, particleID='pos', case=case, edges = edges)._unnormalized_pdf(data_ph)
#pdf = pdf/Integral(pdf, ranges = bounds)
norm_samp  =  UnfilteredSample(1000, bounds)
ntoys = 1
maj        =  EstimateMaximum(sess = sess, pdf = pdf, x = data_ph, norm_sample = norm_samp)
for i in range(ntoys):
    print(seed+i)
    sample_val = RunToyMC(sess = sess, pdf = pdf, x = data_ph, size = int(1644000.0), majorant = maj, ranges = bounds, chunk = 100000, seed = seed+i) 
    print(sample_val.shape)
    write_root(sample_val, '/disk/lhcb_data/amathad/diffdensity/toys/Sample-SM-'+NPmodel+'-'+str(seed+i)+'-4body-partial-'+case+'-binned.root', 'tree', case)
