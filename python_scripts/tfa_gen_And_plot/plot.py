#!/bin/python

import sys, os
os.environ["CUDA_VISIBLE_DEVICES"] = ""
import numpy as np
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
#config  = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
sess = tf.Session(config=config)
from src.model_4body_partial import *
from src.util_tfa import Integral, assign_params, get_max_min, get_wiggled_ffparams
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import pickle
tf.random.set_random_seed(100)
np.random.seed(100)

#label
wilname = {}
wilname['CVR_real'] =r'$V_R$'
wilname['CVL_real'] =r'$V_L$'
wilname['CSR_real'] =r'$S_R$'
wilname['CSL_real'] =r'$S_L$'
wilname['CTL_real'] =r'$T_L$'
wilname['PLB']      =r'$P_{\Lambda_b}$'
wilname['AL']       =r'$\alpha_{\Lambda_c}$'
wilname['FF']       =r'FF'

#label
wilson_mean  = 0.0
wilson_sigma = 0.05
param_limits = {}
param_limits['CVR_real'] =  (wilson_mean, 0.03)
param_limits['CVL_real'] =  (wilson_mean, 0.04)
param_limits['CSR_real'] =  (wilson_mean, 0.46)
param_limits['CSL_real'] =  (wilson_mean, 0.35)
param_limits['CTL_real'] =  (wilson_mean, 0.05)
param_limits['PLB']      =  (0.06, 0.073)
param_limits['AL']       =  (0.18, 0.45)

NPmodel     = sys.argv[1] #Tau, Mu
baseline    = sys.argv[2]
case        = sys.argv[3]
print(NPmodel, case, baseline)

#Params
plb_nom_pt  = 0.
if baseline == 'base_SM_plbM': plb_nom_pt = 0.06
params = {}
params['CVR_real'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CVL_real'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CSR_real'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CSL_real'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CTL_real'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CVR_imag'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CVL_imag'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CSR_imag'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CSL_imag'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['CTL_imag'] = tf.Variable(0.0 , trainable = False, dtype = tf.float64)
params['AL']       = tf.Variable(0.18, trainable = False, dtype = tf.float64)
params['PLB']      = tf.Variable(plb_nom_pt , trainable = False, dtype = tf.float64)
f = open("../FF_cov/LambdabLambdac_results.dat", "r")
for l in f.readlines(): params[l.split()[0]] = tf.Variable(float(l.split()[1]), trainable = False, dtype = tf.float64)
f.close()

#define bounds
ml     = mlepton[NPmodel]
bounds = None
xlabel = r'$q^2$ $(GeV^2/c^4)$'
param_names = []
if 'cthlc' in case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
    ylabel = r'$cos(\theta_{\Lambda_c})$'
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'FF']
elif 'cthl' in case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
    ylabel = r'$cos(\theta_l)$'
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'FF']
elif 'cthp' in case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.))
    ylabel = r'$cos(\theta_p)$'
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'AL', 'FF']
elif 'phl' in case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (0., 2.*pi))
    ylabel = r'$\phi_l$'
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'FF']
elif 'php' in case: 
    bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (0., 2.*pi))
    ylabel = r'$\phi_p$'
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'AL', 'FF']
print(bounds)

#make data grid
grid_size = 1000
q2 = (np.linspace(bounds[0][0], bounds[0][1], grid_size, dtype = np.float64)[1:])[:-1]
ang= (np.linspace(bounds[1][0], bounds[1][1], grid_size, dtype = np.float64)[1:])[:-1]
q2v, angv = np.meshgrid(q2, ang)
shape_arr = q2v.shape
data = np.concatenate([q2v.reshape(q2v.shape[0]**2,1), angv.reshape(angv.shape[0]**2,1)], axis=1)
print(data.shape)

#define model
data_ph = tf.placeholder(tf.float64, shape = (None, 2))
pdf    = MyPdf(name=NPmodel, params=params, particleID='pos', case=case)._unnormalized_pdf(data_ph)
pdf    = pdf/Integral(pdf, ranges = bounds)

#evaluate
init    = tf.global_variables_initializer()
sess.run(init)
fddt = {data_ph: data}
pickle.dump((q2,ang), open('plots/'+baseline+'/'+NPmodel+'/'+case+'/2d.p', "wb"))

#vary one params
sample_pts = 500
for param_name in param_names:
    #define new_pts, paramlist, nominal_pt
    if param_name == 'FF':
        new_pts, paramlist, nominal_pt = get_wiggled_ffparams(size=sample_pts)
    elif param_name == 'PLB':
        new_pts     = np.random.normal(loc=param_limits[param_name][0], scale=param_limits[param_name][1], size=sample_pts)
        paramlist   = [param_name]
        nominal_pt  = [plb_nom_pt]
    else:
        new_pts     = np.random.normal(loc=param_limits[param_name][0], scale=param_limits[param_name][1], size=sample_pts)
        paramlist   = [param_name]
        nominal_pt  = [param_limits[param_name][0]]

    #calculate np and sm pdf values
    pdfnp_list = []
    for new_pt in new_pts:
        if  param_name == 'FF': new_pt_list = new_pt
        else: new_pt_list = [new_pt]
        assign_params(sess, params, new_pt_list, paramlist) #assign new value
        pdfnp = sess.run(pdf, feed_dict = fddt)
        pdfnp_list += [pdfnp.reshape(pdfnp.shape[0], 1)]
    assign_params(sess, params, nominal_pt, paramlist) #assign old value
    pdfsm = sess.run(pdf, feed_dict = fddt)
    pdfsm = np.reshape(pdfsm, shape_arr)

    #define obs 
    pdfnp_max, pdfnp_min = get_max_min(pdfnp_list)
    pdfnp_max  = np.reshape(pdfnp_max, shape_arr)
    pdfnp_min  = np.reshape(pdfnp_min, shape_arr)
    delta_pdfnp= (pdfnp_max - pdfnp_min)
    delta_pdfnp_perc = (delta_pdfnp/pdfsm) * 100.

    #plot
    fig, ax = plt.subplots()
    c= ax.imshow(delta_pdfnp_perc, aspect = 'auto', cmap = 'YlGn', interpolation=None, origin='lower', extent = [q2[0], q2[-1], ang[0], ang[-1]])
    cbar = fig.colorbar(c, ax=ax, fraction=0.046, pad=0.04)
    cbar.set_label('$\Delta (\%)$', fontsize=12)
    ax.set_xlabel(xlabel, fontsize=12)
    ax.set_ylabel(ylabel, fontsize=12)
    if param_name == 'FF':ax.set_title('Vary '+wilname[param_name])
    else: ax.set_title('Vary '+wilname[param_name]+r"$\sim \mathcal{N}"+("(\mu={:1.2f},\sigma={:1.2f})$").format(param_limits[param_name][0],param_limits[param_name][1]), fontsize=14)
    print('plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name+'.pdf')
    fig.savefig('plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name+'.pdf')
    pickle.dump( delta_pdfnp, open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name+'_delta_pdfnp.p', "wb" ) )
    pickle.dump( pdfsm, open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name+'_pdfsm.p', "wb" ) )
