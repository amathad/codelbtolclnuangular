#!/bin/python

import sys, os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import numpy as np
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
from src.model_Lbpol_4body import *
from src.util_tfa import EstimateMaximum, UnfilteredSample, RunToyMC, Integral, write_root

NPmodel     = sys.argv[1] #Tau, Mu
seed = 1

#Wilson coefficients
params = {}
params['CVR_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CVL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CSR_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CSL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CTL_real'] = tf.constant(0.0 , dtype = tf.float64)
params['CVR_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CVL_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CSR_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CSL_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['CTL_imag'] = tf.constant(0.0 , dtype = tf.float64)
params['PLB']      = tf.constant(0.06, dtype = tf.float64)
params['AL']       = tf.constant(0.18, dtype = tf.float64)
f = open("../FF_cov/LambdabLambdac_results.dat", "r")
for l in f.readlines(): params[l.split()[0]] = tf.constant(float(l.split()[1]), dtype = tf.float64)
f.close()

ndim = 6
data_ph = tf.placeholder(tf.float64, shape = (None, ndim))
pdf  = MyPdf(name=NPmodel, params=params, particleID='pos')._unnormalized_pdf(data_ph)
sess = tf.Session(config = config)
ml   = mlepton[NPmodel]
bounds = ((sess.run(ml)**2., sess.run(Mn)**2.), (-1., 1.), (-1., 1.), (0, 2.*pi), (-1.,1.), (0., 2.*pi))  #q2, cthlc, cthl, phl, cthp, php
print(bounds)
pdf = pdf/Integral(pdf, ranges = bounds)
norm_samp  =  UnfilteredSample(5000000, bounds)
maj        =  EstimateMaximum(sess = sess, pdf = pdf, x = data_ph, norm_sample = norm_samp)
ntoys = 1
for i in range(ntoys):
    print(seed+i)
    sample_val = RunToyMC(sess = sess, pdf = pdf, x = data_ph, size = int(1644000.0), majorant = maj, ranges = bounds, chunk = 5000000, seed = seed+i) 
    print(sample_val.shape)
    write_root(sample_val, '/disk/lhcb_data/amathad/diffdensity/toys/Sample-SM-'+NPmodel+'-'+str(seed+i)+'-4body.root', 'tree', case = 'Lbpol4body')
