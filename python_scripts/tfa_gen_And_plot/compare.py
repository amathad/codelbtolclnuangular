import numpy as np
import pickle
import sys

NPmodel     = sys.argv[1] #Tau, Mu
baseline    = sys.argv[2]
case        = sys.argv[3]
print(NPmodel, case, baseline)

param_names = []
if 'cthlc' in case: 
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'FF']
elif 'cthl' in case: 
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'FF']
elif 'cthp' in case: 
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'AL', 'FF']
elif 'phl' in case: 
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'FF']
elif 'php' in case: 
    param_names = ['CVR_real', 'CSL_real', 'CSR_real', 'CTL_real', 'PLB', 'AL', 'FF']

wilsonlist = ['CVR_real', 'CSR_real', 'CTL_real', 'FF']
for param_name_1 in wilsonlist:
    for param_name_2 in wilsonlist:
        if param_name_1 == param_name_2: continue

        delta_pdfnp_1  = pickle.load( open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name_1+'_delta_pdfnp.p', "rb" ) )
        pdfsm_1 = pickle.load( open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name_1+'_pdfsm.p', "rb" ) )

        delta_pdfnp_2 = pickle.load( open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name_2+'_delta_pdfnp.p', "rb" ) )
        pdfsm_2 = pickle.load( open( 'plots/'+baseline+'/'+NPmodel+'/'+case+'/'+param_name_2+'_pdfsm.p', "rb" ) )

        r_np_sm_1 = delta_pdfnp_1/pdfsm_1
        r_np_sm_2 = delta_pdfnp_2/pdfsm_2
        if np.shape(r_np_sm_1) != np.shape(r_np_sm_2):
            print('Shape different exitting'); exit(1)

        #1st
        r_np_sm_max_1 = np.max(r_np_sm_1)
        r_np_sm_min_1 = np.min(r_np_sm_1)
        r_np_sm_1 = (r_np_sm_1 - r_np_sm_min_1)/(r_np_sm_max_1 - r_np_sm_min_1)
        #2nd
        r_np_sm_max_2 = np.max(r_np_sm_2)
        r_np_sm_min_2 = np.min(r_np_sm_2)
        r_np_sm_2 = (r_np_sm_2 - r_np_sm_min_2)/(r_np_sm_max_2 - r_np_sm_min_2)

        print(param_name_1, param_name_2)
        #print(np.mean(((r_np_sm_1-r_np_sm_2)**2)/(np.sqrt(r_np_sm_1**2 + r_np_sm_2**2))))
        print(np.mean(((r_np_sm_1-r_np_sm_2)**2)))
