from .common_params import *

class MyPdf():
    def __init__(self, name, params, particleID):
        self.params    = params
        self.ml        = mlepton[name]
        self.particleID = particleID

    def _unnormalized_pdf(self, data):
        q2       = data[:,0]
        cthlc    = data[:,1] 
        cthl     = data[:,2] 
        phl      = data[:,3]
        cthp     = data[:,4] 
        php      = data[:,5]
        #define normalisation
        pl      = (q2 - self.ml**2)/2./tf.sqrt(q2)
        plc     = plambda(MLb**2, MLc**2, q2)/2./MLb
        N       = (pl * BF_Lc * plc * GF**2 * Vcb**2)/(2.**12 * pi**5 * MLb**2 * tf.sqrt(q2))
        #Dynamic term
        Asq = self.AmplSq(q2, cthlc, cthl, phl, cthp, php)
        #differential density
        diffdecay =  N * Asq 
        return Tau_Lb_gev * diffdecay 

    def AmplSq(self, q2, cthlc, cthl, phl, cthp, php):
        #flav of Lb
        flav = 1.
        if self.particleID == 'neg': flav = -1.
        php = flav*php
        phl = flav*phl
        #polarisation
        plb = self.params['PLB']  
        #decay asymmetry
        alc = CastComplex(self.params['AL'])
        #Wilson coefficients  - Complex
        vl_re  = self.params['CVL_real'] 
        vr_re  = self.params['CVR_real'] 
        sl_re  = self.params['CSL_real']
        sr_re  = self.params['CSR_real']
        tl_re  = self.params['CTL_real']  
        vl_im  = self.params['CVL_imag'] 
        vr_im  = self.params['CVR_imag'] 
        sl_im  = self.params['CSL_imag']
        sr_im  = self.params['CSR_imag']
        tl_im  = self.params['CTL_imag']  
        vl     = CastComplex(tf.complex(vl_re, vl_im))
        vr     = CastComplex(tf.complex(vr_re, vr_im))
        sl     = CastComplex(tf.complex(sl_re, sl_im))
        sr     = CastComplex(tf.complex(sr_re, sr_im))
        g_TL   = CastComplex(tf.complex(tl_re, tl_im))
        g_1PvlMvr = (1. + vl - vr)
        g_1PvlPvr = (1. + vl + vr)
        g_SLPSR   = (sl + sr)
        g_SLMSR   = (sl - sr)
        #define preliminary - Real
        Q_p = Mp**2 - q2 
        Q_n = Mn**2 - q2 
        sqrtQp = tf.sqrt(Q_p)
        sqrtQn = tf.sqrt(Q_n)
        v = tf.sqrt(1. - self.ml**2/q2)
        fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp = self.GetFormFactors(q2) #Form factors
        #define q2 dependent terms - CastComplex
        aV  = CastComplex((f0 * sqrtQp * Mn)/tf.sqrt(q2))
        bV  = CastComplex((fplus * sqrtQn * Mp)/tf.sqrt(q2))
        cV  = CastComplex(CastFloat(tf.sqrt(2.)) * fperp * sqrtQn)
        aA  = CastComplex((g0 * sqrtQn * Mp)/tf.sqrt(q2))
        bA  = CastComplex((gplus * sqrtQp * Mn)/tf.sqrt(q2))
        cA  = CastComplex(CastFloat(tf.sqrt(2.)) * gperp * sqrtQp)
        aS  = CastComplex((f0 * sqrtQp * Mn)/(mb - mc))
        aP  = CastComplex((g0 * sqrtQn * Mp)/(mb + mc))
        aT  = CastComplex(hplus * sqrtQn)
        bT  = CastComplex((CastFloat(tf.sqrt(2.)) * hperp  * Mp * sqrtQn)/tf.sqrt(q2))
        cT  = CastComplex((CastFloat(tf.sqrt(2.)) * htperp * Mn * sqrtQp)/tf.sqrt(q2))
        dT  = CastComplex(htplus * sqrtQp)
        al  = CastComplex(2. * self.ml * v)
        bl  = CastComplex(2. * tf.sqrt(q2) * v)
        #Define I_i terms - Should be complex
        I1  = (-4.*bl*(bT + cT)*g_TL + al*cA*g_1PvlMvr - al*cV*g_1PvlPvr)
        I2  = ( 4.*bl*(bT - cT)*g_TL + al*cA*g_1PvlMvr + al*cV*g_1PvlPvr)
        I3  = ( 4.*al*(bT - cT)*g_TL + bl*cA*g_1PvlMvr + bl*cV*g_1PvlPvr)
        I4  = (-4.*al*(bT + cT)*g_TL + bl*cA*g_1PvlMvr - bl*cV*g_1PvlPvr)
        I5  = ( 4.*bl*(aT - dT)*g_TL + al*bA*g_1PvlMvr + al*bV*g_1PvlPvr)
        I6  = (-4.*bl*(aT + dT)*g_TL + al*bA*g_1PvlMvr - al*bV*g_1PvlPvr)
        I7  = ( 4.*al*(aT + dT)*g_TL - bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I8  = ( 4.*al*(aT - dT)*g_TL + bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I9  = (-(aP*bl*g_SLMSR) + aS*bl*g_SLPSR + aA*al*g_1PvlMvr + al*aV*g_1PvlPvr)
        I10 = (-(aP*bl*g_SLMSR) - aS*bl*g_SLPSR + aA*al*g_1PvlMvr - al*aV*g_1PvlPvr)
        #decay asymmetry
        absgpsq = (1. + alc)/2.
        absgnsq = (1. - alc)/2.
        #K_i terms: Cast data to be complex
        cthl     = CastComplex(cthl )
        phl      = CastComplex(phl  )
        cthp     = CastComplex(cthp )
        php      = CastComplex(php  )
        K1 =  tf.real(((absgnsq - absgpsq)*tf.sqrt(1. -cthl**2)*(-(tf.exp(2.*1j*phl)*I8*tf.conj(I4)*(1. +cthl)) +  \
              tf.exp(2.*1j*phl)*tf.conj(I1)*(I9 + I5*cthl) +  \
              tf.exp(2.*1j*php)*(-(I4*tf.conj(I8)*(1. +cthl)) + I1*(tf.conj(I9) + tf.conj(I5)*cthl)))*tf.sqrt(1. -cthp**2))/(2.*CastComplex(tf.sqrt(2.))*tf.exp(1j*(phl + php))) +  \
              (absgnsq*(I1*tf.conj(I1) + 3*I4*tf.conj(I4) + 2.*I8*tf.conj(I8) + 4.*I9*tf.conj(I9)) +  \
              absgpsq*(I1*tf.conj(I1) + 3*I4*tf.conj(I4) + 2.*I5*tf.conj(I5) + 2.*I8*tf.conj(I8) + 4.*I9*tf.conj(I9)) +  \
              4.*(absgnsq + absgpsq)*(I4*tf.conj(I4) + I9*tf.conj(I5) + I5*tf.conj(I9))*cthl +  \
              (absgnsq + absgpsq)*(-I1*tf.conj(I1) + I4*tf.conj(I4) + 2.*I5*tf.conj(I5) - 2.*I8*tf.conj(I8))*(-1. +2.*cthl**2) +  \
              (absgnsq - absgpsq)*(-2.*I4*tf.conj(I4)*(1. +cthl)**2 + 4.*tf.conj(I9)*(I9 + I5*cthl) -  \
              2.*(I1*tf.conj(I1) - 2.*I8*tf.conj(I8))*(1. -cthl**2) + 2.*tf.conj(I5)*(2.*I9*cthl + I5*(-1. +2.*cthl**2)))*cthp +  \
              2.*I5*tf.conj(I5)*(-(absgpsq*cthp) + absgnsq*(1. +cthp)))/8.);
        K2 =  tf.real((2.*absgnsq*(4.*(tf.conj(I6)*cthl*(I10 + I6*cthl) + I7*tf.conj(I7)*(1. -cthl**2))*(1. -cthp) - \
              4.*tf.conj(I10)*(I10 + I6*cthl)*(-1. +cthp) + \
              2.*(I3*tf.conj(I3)*(1. -cthl)**2 + I2*tf.conj(I2)*(1. -cthl**2))*(1. +cthp)) + \
              (4.*CastComplex(tf.sqrt(2.))*(absgnsq - absgpsq)*tf.sqrt(1. -cthl**2)*\
              (-(tf.exp(2.*1j*phl - 1j*php)*(I2*tf.conj(I10) + I3*tf.conj(I7)*(-1. +cthl) + I2*tf.conj(I6)*cthl)) - \
              tf.exp(1j*php)*(I7*tf.conj(I3)*(-1. +cthl) + tf.conj(I2)*(I10 + I6*cthl)))*tf.sqrt(1. -cthp**2) + \
              4.*tf.exp(1j*phl)*absgpsq*(I3*tf.conj(I3)*(1. -cthl)**2*(1. -cthp) + \
              2.*(I10 + I6*cthl)*(tf.conj(I10) + tf.conj(I6)*cthl)*(1. +cthp) + \
              (1. -cthl**2)*(2.*I7*tf.conj(I7)*(1. +cthp) + tf.conj(I2)*(I2 - I2*cthp))))/tf.exp(1j*phl))/16.);
        K3 =  tf.real(((4.*CastComplex(tf.sqrt(2.))*(absgnsq + absgpsq)*tf.sqrt(1. -cthl**2)* \
              (-(I1*tf.conj(I10)) + I9*tf.conj(I2) + I8*tf.conj(I3) - I4*tf.conj(I7) +  \
              (I5*tf.conj(I2) - I8*tf.conj(I3) - I1*tf.conj(I6) - I4*tf.conj(I7))*cthl +  \
              tf.exp(2.*1j*phl)*(-(I3*tf.conj(I8)*(-1. +cthl)) - I7*tf.conj(I4)*(1. +cthl) - tf.conj(I1)*(I10 + I6*cthl) +  \
              I2*(tf.conj(I9) + tf.conj(I5)*cthl))))/tf.exp(1j*phl) +  \
              (4.*CastComplex(tf.sqrt(2.))*(absgnsq - absgpsq)*tf.sqrt(1. -cthl**2)* \
              (I1*tf.conj(I10) + I9*tf.conj(I2) + I8*tf.conj(I3) + I4*tf.conj(I7) +  \
              (I5*tf.conj(I2) - I8*tf.conj(I3) + I1*tf.conj(I6) + I4*tf.conj(I7))*cthl +  \
              tf.exp(2.*1j*phl)*(-(I3*tf.conj(I8)*(-1. +cthl)) + I7*tf.conj(I4)*(1. +cthl) + tf.conj(I1)*(I10 + I6*cthl) +  \
              I2*(tf.conj(I9) + tf.conj(I5)*cthl)))*cthp)/tf.exp(1j*phl) +  \
              (4.*(absgnsq - absgpsq)*(tf.exp(2.*1j*phl)*(-2.*(I9 + I5*cthl)*(tf.conj(I10) + tf.conj(I6)*cthl) +  \
              (tf.exp(2.*1j*phl)*(I2*tf.conj(I1) - I3*tf.conj(I4)) + 2.*I8*tf.conj(I7))*(1. -cthl**2)) +  \
              tf.exp(2.*1j*php)*((I1*tf.conj(I2) - I4*tf.conj(I3))*(1. -cthl**2) -  \
              2.*tf.exp(2.*1j*phl)*((I10 + I6*cthl)*(tf.conj(I9) + tf.conj(I5)*cthl) - I7*tf.conj(I8)*(1. -cthl**2))))* \
              tf.sqrt(1. -cthp**2))/tf.exp(1j*(2.*phl + php)))/16.);
        #ampsq
        Asq = (K1 * (1. - plb * cthlc) + K2 * (1. +plb * cthlc) + K3 * plb * tf.sqrt(1. - cthlc**2))
        return Asq

    def GetFormFactors(self, q2):
        #Weinber FF
        fplus = ff(q2, mf_pole(Deltaf['fplus']),  self.params['a0_fplus'], self.params['a1_fplus'])
        fperp = ff(q2, mf_pole(Deltaf['fperp']),  self.params['a0_fperp'], self.params['a1_fperp'])
        f0    = ff(q2, mf_pole(Deltaf['f0']),     self.params['a0_f0']   , self.params['a1_f0']   )
        gplus = ff(q2, mf_pole(Deltaf['gplus']),  self.params['a0_gpp'], self.params['a1_gplus'])
        gperp = ff(q2, mf_pole(Deltaf['gperp']),  self.params['a0_gpp'], self.params['a1_gperp'])
        g0    = ff(q2, mf_pole(Deltaf['g0']),     self.params['a0_g0']   , self.params['a1_g0']   )
        hplus = ff(q2, mf_pole(Deltaf['hplus']),  self.params['a0_hplus'], self.params['a1_hplus'])
        hperp = ff(q2, mf_pole(Deltaf['hperp']),  self.params['a0_hperp'], self.params['a1_hperp'])
        htplus= ff(q2, mf_pole(Deltaf['htplus']), self.params['a0_htildepp'],self.params['a1_htildeplus'])
        htperp= ff(q2, mf_pole(Deltaf['htperp']), self.params['a0_htildepp'],self.params['a1_htildeperp'])
        return (fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp)
