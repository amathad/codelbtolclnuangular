from .common_params import *

class MyPdf():
    def __init__(self, name, params, case, particleID):
        self.params    = params
        self.ml        = mlepton[name]
        self.case      = case
        self.particleID = particleID

    def _unnormalized_pdf(self, data):
        diffdecay =  self.diff_decay(data)
        return Tau_Lb_gev * diffdecay

    def diff_decay(self, data):
        q2         = data[:,0]
        angle      = data[:,1] 
        angle2     = None
        if self.case == 'q2cthpphp': angle2 = data[:,1] 
        #define normalisation
        pl      = (q2 - self.ml**2)/2./tf.sqrt(q2)
        plc     = plambda(MLb**2, MLc**2, q2)/2./MLb
        N       = (pl * BF_Lc * plc * GF**2 * Vcb**2)/(2.**12 * pi**5 * MLb**2 * tf.sqrt(q2))
        #flav of Lb - only used in the case of azimuthal angle
        flav = 1.
        if self.particleID == 'neg': flav = -1.
        #Wilson coefficients  - Complex
        vl_re  = self.params['CVL_real'] 
        vr_re  = self.params['CVR_real'] 
        sl_re  = self.params['CSL_real']
        sr_re  = self.params['CSR_real']
        tl_re  = self.params['CTL_real']  
        vl_im  = self.params['CVL_imag'] 
        vr_im  = self.params['CVR_imag'] 
        sl_im  = self.params['CSL_imag']
        sr_im  = self.params['CSR_imag']
        tl_im  = self.params['CTL_imag']  
        vl     = CastComplex(tf.complex(vl_re, vl_im))
        vr     = CastComplex(tf.complex(vr_re, vr_im))
        sl     = CastComplex(tf.complex(sl_re, sl_im))
        sr     = CastComplex(tf.complex(sr_re, sr_im))
        g_TL   = CastComplex(tf.complex(tl_re, tl_im))
        g_1PvlMvr = (1. + vl - vr)
        g_1PvlPvr = (1. + vl + vr)
        g_SLPSR   = (sl + sr)
        g_SLMSR   = (sl - sr)
        #define preliminary - Real
        Q_p = Mp**2 - q2 
        Q_n = Mn**2 - q2 
        sqrtQp = tf.sqrt(Q_p)
        sqrtQn = tf.sqrt(Q_n)
        v = tf.sqrt(1. - self.ml**2/q2)
        fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp = self.GetFormFactors(q2) #Form factors
        #define q2 dependent terms - CastComplex
        aV  = CastComplex((f0 * sqrtQp * Mn)/tf.sqrt(q2))
        bV  = CastComplex((fplus * sqrtQn * Mp)/tf.sqrt(q2))
        cV  = CastComplex(CastFloat(tf.sqrt(2.)) * fperp * sqrtQn)
        aA  = CastComplex((g0 * sqrtQn * Mp)/tf.sqrt(q2))
        bA  = CastComplex((gplus * sqrtQp * Mn)/tf.sqrt(q2))
        cA  = CastComplex(CastFloat(tf.sqrt(2.)) * gperp * sqrtQp)
        aS  = CastComplex((f0 * sqrtQp * Mn)/(mb - mc))
        aP  = CastComplex((g0 * sqrtQn * Mp)/(mb + mc))
        aT  = CastComplex(hplus * sqrtQn)
        bT  = CastComplex((CastFloat(tf.sqrt(2.)) * hperp  * Mp * sqrtQn)/tf.sqrt(q2))
        cT  = CastComplex((CastFloat(tf.sqrt(2.)) * htperp * Mn * sqrtQp)/tf.sqrt(q2))
        dT  = CastComplex(htplus * sqrtQp)
        al  = CastComplex(2. * self.ml * v)
        bl  = CastComplex(2. * tf.sqrt(q2) * v)
        #Define I_i terms - Should be complex
        I1  = (-4.*bl*(bT + cT)*g_TL + al*cA*g_1PvlMvr - al*cV*g_1PvlPvr)
        I2  = ( 4.*bl*(bT - cT)*g_TL + al*cA*g_1PvlMvr + al*cV*g_1PvlPvr)
        I3  = ( 4.*al*(bT - cT)*g_TL + bl*cA*g_1PvlMvr + bl*cV*g_1PvlPvr)
        I4  = (-4.*al*(bT + cT)*g_TL + bl*cA*g_1PvlMvr - bl*cV*g_1PvlPvr)
        I5  = ( 4.*bl*(aT - dT)*g_TL + al*bA*g_1PvlMvr + al*bV*g_1PvlPvr)
        I6  = (-4.*bl*(aT + dT)*g_TL + al*bA*g_1PvlMvr - al*bV*g_1PvlPvr)
        I7  = ( 4.*al*(aT + dT)*g_TL - bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I8  = ( 4.*al*(aT - dT)*g_TL + bA*bl*g_1PvlMvr + bl*bV*g_1PvlPvr)
        I9  = (-(aP*bl*g_SLMSR) + aS*bl*g_SLPSR + aA*al*g_1PvlMvr + al*aV*g_1PvlPvr)
        I10 = (-(aP*bl*g_SLMSR) - aS*bl*g_SLPSR + aA*al*g_1PvlMvr - al*aV*g_1PvlPvr)
        if self.case == 'q2cthlc': 
            #no alc deped, and when plb = 0 dependence on cthlc is removed
            plb = CastComplex(self.params['PLB'])
            cthlc   = CastComplex(angle)
            Asq = (4.*pi**2*((1. + cthlc*plb)*(I2*tf.conj(I2) + I6*tf.conj(I6) + 3.*I10*tf.conj(I10) + 2.*I3*tf.conj(I3) + 2.*I7*tf.conj(I7)) - \
                            (-1. + cthlc*plb)*(I1*tf.conj(I1) + I5*tf.conj(I5) + 2.*I4*tf.conj(I4) + 2.*I8*tf.conj(I8) + 3.*I9*tf.conj(I9))))/3.;
        elif self.case == 'q2cthl':
            #no dependence on plb or alc
            cthl   = CastComplex(angle)
            Asq  = 2.*pi**2*(I2*tf.conj(I2) + I3*tf.conj(I3) + I4*tf.conj(I4) + (I1 - cthl**2*I1)*tf.conj(I1) + 2.*(I10 + cthl*I6)*tf.conj(I10) + \
                            cthl*(-2.*cthl*(I7*tf.conj(I7) + I8*tf.conj(I8)) - cthl*I2*tf.conj(I2) + (-2. + cthl)*I3*tf.conj(I3) + (2. + cthl)*I4*tf.conj(I4) + \
                            2.*(cthl*I5 + I9)*tf.conj(I5) + 2.*(I10 + cthl*I6)*tf.conj(I6)) + 2.*I7*tf.conj(I7) + 2.*I8*tf.conj(I8) + \
                            2.*(cthl*I5 + I9)*tf.conj(I9));
        elif self.case == 'q2phl':
            plb = CastComplex(self.params['PLB'])
            #no alc deped, and when plb = 0 dependence on phl removed
            phl   = CastComplex(flav*angle)
            Asq   =  (pi*(16.*I1*tf.conj(I1) + 48.*I10*tf.conj(I10) + 16.*I2*tf.conj(I2) + 32.*I3*tf.conj(I3) + 32.*I4*tf.conj(I4) + \
                             16.*I5*tf.conj(I5) + 16.*I6*tf.conj(I6) + 32.*I7*tf.conj(I7) + 32.*I8*tf.conj(I8) + 48.*I9*tf.conj(I9) + \
                             (3.*pi**2*plb*(-(I1*tf.conj(I10)) + I9*tf.conj(I2) + I8*tf.conj(I3) - I4*tf.conj(I7) + tf.exp(2.*1j*phl)*(-(I10*tf.conj(I1)) - \
                             I7*tf.conj(I4) + I3*tf.conj(I8) + I2*tf.conj(I9))))/(CastComplex(tf.sqrt(2.))*tf.exp(1j*phl))))/12.;
        elif self.case == 'q2cthp':
            alc = CastComplex(self.params['AL'])
            #no plb dependence, and when alc = 0 dependence on cthp removed
            cthp   = CastComplex(angle)
            Asq  = (4.*pi**2*(I2*tf.conj(I2) + I5*tf.conj(I5) + I6*tf.conj(I6) + 2.*(I3*tf.conj(I3) + I4*tf.conj(I4) + I7*tf.conj(I7) + I8*tf.conj(I8)) + \
                            (I1 + alc*cthp*I1)*tf.conj(I1) + 3.*(1. + alc*cthp)*I10*tf.conj(I10) + 3.*I9*tf.conj(I9) - \
                            alc*cthp*(I2*tf.conj(I2) + I5*tf.conj(I5) + 2.*I3*tf.conj(I3) - 2.*I4*tf.conj(I4) - I6*tf.conj(I6) - 2.*I7*tf.conj(I7) + \
                            2.*I8*tf.conj(I8) + 3.*I9*tf.conj(I9))))/3.;
        elif self.case == 'q2php':
            plb = CastComplex(self.params['PLB'])
            alc = CastComplex(self.params['AL'])
            #depends on alc and plb and if either one of them is zero then php dependence removed
            php   = CastComplex(flav*angle)
            Asq   =  (pi*(8.*I1*tf.conj(I1) + 24.*I10*tf.conj(I10) + 8.*I2*tf.conj(I2) + 16.*I3*tf.conj(I3) + 16.*I4*tf.conj(I4) + 8.*I5*tf.conj(I5) +\
                             8.*I6*tf.conj(I6) + 16.*I7*tf.conj(I7) + 16.*I8*tf.conj(I8) + 24.*I9*tf.conj(I9) +\
                             (alc*pi**2*plb*(3.*I9*tf.conj(I10) + I5*tf.conj(I6) - 2.*I8*tf.conj(I7) +\
                             tf.exp(2.*1j*php)*(I6*tf.conj(I5) - 2.*I7*tf.conj(I8) + 3.*I10*tf.conj(I9))))/(2.*tf.exp(1j*php))))/6.;
        elif self.case == 'q2cthpphp':
            #depends on alc and plb 
            plb = CastComplex(self.params['PLB'])
            alc = CastComplex(self.params['AL'])
            cthp  = CastComplex(flav*angle)
            php   = CastComplex(flav*angle2)
            Asq   = (pi*(2.*I1*tf.conj(I1) + 2.*alc*cthp*I1*tf.conj(I1) + 6.*I10*tf.conj(I10) + 6.*alc*cthp*I10*tf.conj(I10) + 2.*I2*tf.conj(I2) - \
                    2.*alc*cthp*I2*tf.conj(I2) + 4.*I3*tf.conj(I3) - 4.*alc*cthp*I3*tf.conj(I3) + 4.*I4*tf.conj(I4) + 4.*alc*cthp*I4*tf.conj(I4) + \
                    2.*I5*tf.conj(I5) - 2.*alc*cthp*I5*tf.conj(I5) + 2.*I6*tf.conj(I6) + 2.*alc*cthp*I6*tf.conj(I6) + 4.*I7*tf.conj(I7) + \
                    4.*alc*cthp*I7*tf.conj(I7) + 4.*I8*tf.conj(I8) - 4.*alc*cthp*I8*tf.conj(I8) + 6.*I9*tf.conj(I9) - 6.*alc*cthp*I9*tf.conj(I9) + \
                    (alc*tf.sqrt(1. - cthp**2)*pi*plb*(3.*I9*tf.conj(I10) + I5*tf.conj(I6) - 2.*I8*tf.conj(I7) + \
                    tf.exp(2.*1j*php)*(I6*tf.conj(I5) - 2.*I7*tf.conj(I8) + 3.*I10*tf.conj(I9))))/(2.*tf.exp(1j*php))))/3.;
        else:
            print(self.case, 'does not exist')
            exit(1)

        Asq = tf.real(Asq)
        return N * Asq

    def GetFormFactors(self, q2):
        #Weinber FF
        fplus = ff(q2, mf_pole(Deltaf['fplus']),  self.params['a0_fplus'], self.params['a1_fplus'])
        fperp = ff(q2, mf_pole(Deltaf['fperp']),  self.params['a0_fperp'], self.params['a1_fperp'])
        f0    = ff(q2, mf_pole(Deltaf['f0']),     self.params['a0_f0']   , self.params['a1_f0']   )
        gplus = ff(q2, mf_pole(Deltaf['gplus']),  self.params['a0_gpp'], self.params['a1_gplus'])
        gperp = ff(q2, mf_pole(Deltaf['gperp']),  self.params['a0_gpp'], self.params['a1_gperp'])
        g0    = ff(q2, mf_pole(Deltaf['g0']),     self.params['a0_g0']   , self.params['a1_g0']   )
        hplus = ff(q2, mf_pole(Deltaf['hplus']),  self.params['a0_hplus'], self.params['a1_hplus'])
        hperp = ff(q2, mf_pole(Deltaf['hperp']),  self.params['a0_hperp'], self.params['a1_hperp'])
        htplus= ff(q2, mf_pole(Deltaf['htplus']), self.params['a0_htildepp'],self.params['a1_htildeplus'])
        htperp= ff(q2, mf_pole(Deltaf['htperp']), self.params['a0_htildepp'],self.params['a1_htildeperp'])
        return (fplus, fperp, f0, gplus, gperp, g0, hplus, hperp, htplus, htperp)
