import numpy as np
import tensorflow as tf
import functools

def check_symmetric(a, rtol=1e-05, atol=1e-08): return np.allclose(a, a.T, rtol=rtol, atol=atol)

def is_symm_pos_def(A):
    if check_symmetric(A):
        try:
            np.linalg.cholesky(A)
            return True
        except np.linalg.LinAlgError:
            return False
    else:
        return False

def Integral(pdf = None, ranges = None): return tf.reduce_mean(pdf) * functools.reduce(lambda x, y: x*y, [r[1] - r[0] for r in ranges])

def EstimateMaximum(sess = None, pdf = None, x = None, norm_sample = None): return np.nanmax( sess.run(pdf, { x : norm_sample } ) )

def UnfilteredSample(size = None, ranges = None, majorant = -1) :
  v = [ np.random.uniform(r[0], r[1], size ).astype('d') for r in ranges ]
  if majorant>0 : v += [ np.random.uniform( 0., majorant, size).astype('d') ]
  return np.transpose(np.array(v))

def CreateAcceptRejectSample(sess = None, density = None, x = None, sample = None) : 
  p = sample[:,0:-1]
  r = sample[:,-1]
  pdf_data = sess.run(density, feed_dict = {x : p})
  return p[ pdf_data > r ]

def RunToyMC(sess = None, pdf = None, x = None, size = None, majorant = None, chunk = 200000, switches = None, seed = None, ranges = None) : 
  first = True
  length = 0
  nchunk = 0
  if seed : np.random.seed(seed)
  while length < size or nchunk < -size : 
    d1 = UnfilteredSample(size = chunk, ranges = ranges, majorant = majorant)
    d = CreateAcceptRejectSample(sess = sess, density = pdf, x = x, sample = d1)
    if first : data = d
    else : data = np.append(data, d, axis = 0)
    first = False
    length += len(d)
    nchunk += 1
    print("  Chunk %d, size=%d, total length=%d" % (nchunk, len(d), length))
  if size > 0 : 
    return data[:size]
  else : 
    return data

import pandas as pd
from root_pandas import to_root, read_root

def assign_params(sess, param_dict, param_list_newpts, param_list_names):
    print('Assigning param names', param_list_names)
    print('Assigning values', param_list_newpts)
    for fname, fval in zip(param_list_names, param_list_newpts): param_dict[fname].load(fval, sess)
    return None

def get_wiggled_ffparams(size):
    #get mean
    mean = {}
    for l in open("../FF_cov/LambdabLambdac_results.dat", "r").readlines(): mean[l.split()[0]] = float(l.split()[1])
    names = mean.keys()
    mean_list = [mean[n] for n in names]
    #get cov
    covariance = {}
    for l in names: covariance[l] = {}
    for l in open("../FF_cov/LambdabLambdac_covariance.dat", "r").readlines(): covariance[l.split()[0]][l.split()[1]] = float(l.split()[2])
    cov_list = [[covariance[l1][l2] for l2 in names] for l1 in names]
    #convert to arrays
    mean_list = np.array(mean_list)
    cov_list  = np.array(cov_list)
    #check if symmetric and positive definite
    if not is_symm_pos_def(cov_list): 
        print('Not symmetric positive definite cov matrix, exiting')
        exit(0)
    else:
        print('Cov is symmetric and positive definite')
    ff_params_vals = np.random.multivariate_normal(mean_list, cov_list, size)
    return ff_params_vals, names, mean_list

def write_root(sample_val, fname, tname, case):
    if case == 'Lbpol4body':
        df = pd.DataFrame({\
        'q2':    sample_val[:,0], \
        'cthlc': sample_val[:,1], \
        'cthl':  sample_val[:,2], \
        'phl':   sample_val[:,3], \
        'cthp':  sample_val[:,4], \
        'php':   sample_val[:,5]
        })
    elif case == 'Lbpol3body':
        df = pd.DataFrame({\
        'q2':      sample_val[:,0], \
        'cthlc':   sample_val[:,1], \
        'cthl':    sample_val[:,2], \
        'phl':     sample_val[:,3]
        })
    elif case == 'q2cthpphp':
        df = pd.DataFrame({\
        'q2':      sample_val[:,0], \
        'cthp':    sample_val[:,1], \
        'php':     sample_val[:,2] 
        })
    else:
        df = pd.DataFrame({'q2':      sample_val[:,0], case.split('2')[1]:   sample_val[:,1]})

    print(df.shape)
    print(fname, tname)
    df.to_root(fname, key=tname)
    return None

def get_pdfval_center(h, edges):
    pdf_val = []
    center  = []
    if len(edges) > 1: #only 2d
        for xbin in range(edges[0].size -1):
            for ybin in range(edges[1].size -1):
                pdf_val += [h[xbin][ybin]]
                xcenter = edges[0][xbin] + (edges[0][-1] - edges[0][0])/(2. * (edges[0].size - 1))
                ycenter = edges[1][ybin] + (edges[1][-1] - edges[1][0])/(2. * (edges[1].size - 1))
                center  += [(xcenter, ycenter)]
    else:
        for xbin in range(len(edges[0]) - 1):
            pdf_val += [h[xbin]]
            xcenter = edges[0][xbin] + (edges[0][-1] - edges[0][0])/(2. * (edges[0].size - 1))
            center  += [(xcenter)]

    return np.array(pdf_val), np.array(center)

def get_pdfs(filename, xbins, ybins, xbins_1d, ybins_1d, bounds):
    #get file
    print(filename)
    df = read_root(filename, 'tree', columns = ['q2', 'costhl'])
    sample = np.concatenate([df['q2'].values.reshape(df.shape[0],1), df['costhl'].values.reshape(df.shape[0],1)], axis=1)
    #2d
    pdf, edges = np.histogramdd(sample, bins = (xbins, ybins), range = bounds)
    pdf = pdf/np.sum(pdf)
    pdf, centers = get_pdfval_center(pdf, edges) 
    print('len(pdf)', len(pdf))
    print('sum(pdf)', sum(pdf))
    #q2 
    pdf_q2, edges_q2 = np.histogramdd(df['q2'].values.reshape(df.shape[0],1), bins = (xbins_1d,), range = (bounds[0],))
    pdf_q2 = pdf_q2/np.sum(pdf_q2)
    pdf_q2, centers_q2 = get_pdfval_center(pdf_q2, edges_q2) 
    print('len(pdf)', len(pdf_q2))
    print('sum(pdf)', sum(pdf_q2))
    #costhl 
    pdf_costhl, edges_costhl = np.histogramdd(df['costhl'].values.reshape(df.shape[0],1), bins = (ybins_1d,), range = (bounds[1],))
    pdf_costhl = pdf_costhl/np.sum(pdf_costhl)
    pdf_costhl, centers_costhl = get_pdfval_center(pdf_costhl, edges_costhl) 
    print('len(pdf)', len(pdf_costhl))
    print('sum(pdf)', sum(pdf_costhl))
    return ((pdf, centers), (pdf_q2, centers_q2), (pdf_costhl, centers_costhl))

def get_max_min(pdfs):
    pdf_full = np.concatenate(pdfs, axis=1)
    pdf_max  = np.amax(pdf_full, axis = 1)
    pdf_min  = np.amin(pdf_full, axis = 1)
    return pdf_max, pdf_min

def get_1d_pdf(filename, bins, bounds, cut_ranges, proj):
    if proj == 'costhl': 
        selection = 'q2>'+str(cut_ranges[0])+'&&'+'q2<'+str(cut_ranges[1])
        plot_range = bounds[1]
    else:
        selection = 'costhl>'+str(cut_ranges[0])+'&&'+'costhl<'+str(cut_ranges[1])
        plot_range = bounds[0]
    print(filename, proj, selection, plot_range)
    df = read_root(filename, 'tree', columns = [proj], where = selection)
    print(df.shape)
    pdf, edges = np.histogramdd(df[proj].values.reshape(df.shape[0],1), bins = (bins,), range = (plot_range,))
    pdf = pdf/np.sum(pdf)
    pdf, centers = get_pdfval_center(pdf, edges) 
    return (pdf, centers)
