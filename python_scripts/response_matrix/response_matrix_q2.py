#!/bin/python
from root_pandas import read_root
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
import sys

case = sys.argv[1]
bins_str = sys.argv[2]

#define cut
cut='abs(Lb_TRUEID)==5122&&abs(Lc_TRUEID)==4122&&abs(p_TRUEID)==2212&&abs(K_TRUEID)==321&&abs(pi_TRUEID)==211&&abs(mu_TRUEID)==13&&Lb_BKGCAT<60&&'
cut+='Lb_True_Q2_mu>11164.0356&&Lb_True_Q2_mu<11109822.259600001&&Lb_Q2BEST>11164.0356&&Lb_Q2BEST<11109822.259600001&&Lb_True_Costhetal_mu>-1.&&Lb_True_Costhetal_mu<1.&&Lb_COSTHETALBEST>-1.&&Lb_COSTHETALBEST<1.'
print(cut)
bins_reco   = [int(bins_str)]
bins_true   = [int(bins_str)]
bins = bins_reco + bins_true
print(bins)
q2_lim      = (0.0111640356, 11.109822259600001)
limits      = (q2_lim, q2_lim)
print(limits)
lget           = ['noexpand:Lb_Q2BEST/1e6', 'noexpand:Lb_True_Q2_mu/1e6']
ldict          = {'Lb_Q2BEST/1e6':'q2reco', 'Lb_True_Q2_mu/1e6':'q2true'}
df=read_root('/disk/lhcb_data/mferrill/Lcmunu/MC2016/tupleout_background.root', key='tupleout/DecayTree', where=cut, columns=lget) #shape (ndata, 4) 
df=df.rename(columns=ldict)
l              = ['q2reco', 'q2true']

print(df)
print(df[l].values.shape)
mij_norm, edges = np.histogramdd(df[l].values, bins=bins, range=limits) #ALWAYS PASS LIST OF KEYS WHEN CALLING VALUES
print(mij_norm.sum())
mij_norm = mij_norm/mij_norm.sum()
print(mij_norm.sum())
print(mij_norm.shape)
f = lambda a: (a[:-1] + a[1:])/2. 
A, B = f(edges[0]), f(edges[1])
print('A', A)
print('B', B)

nj = np.einsum('ij->j', mij_norm) #should be already normalised, sum over i (reco)
print(nj.sum())
mij_new = mij_norm/nj #convert to probability that a given true value in bin lie in each of the reco bins
nj_new = np.einsum('ij->j', mij_new); print(nj_new) #should all be one

import pickle
pickle.dump(mij_new, open('/disk/lhcb_data/amathad/diffdensity/responsematrix/'+case+'-'+bins_str+'.p', 'wb'))

nmc_true, _ = np.histogram(df[l[1]].values, bins=edges[1], range=q2_lim)
nmc_true = nmc_true/nmc_true.sum()
print(nmc_true.sum())
nmc_reco, _ = np.histogram(df[l[0]].values, bins=edges[0], range=q2_lim)
nmc_reco = nmc_reco/nmc_reco.sum()
print(nmc_reco.sum())

l_toys = ['q2']
df_toys = read_root('/disk/lhcb_data/amathad/diffdensity/toys/Sample-SM-Mu-1-4body-partial-'+case+'.root', key='tree', columns = l_toys)
print(df_toys.shape)
ntrue, _ = np.histogram(df_toys[l_toys[0]].values, bins=edges[1], range=q2_lim)
ntrue = ntrue/ntrue.sum()
print(ntrue.sum())
nreco = np.einsum('ij,j->i', mij_new, ntrue) #should be normalised
print(nreco.sum())

##closure test
#ntrue = nj
#nreco = np.einsum('ij,j->i', mij_new, ntrue) #should be normalised

#if bins_str == '20':
#    ###############
#    fig, axs = plt.subplots(2, 2)
#    
#    ax = axs[0,0]
#    c  = ax.plot(nmc_true)
#    ax.set_title('MC True')
#    
#    ax = axs[0,1]
#    c  = ax.plot(nmc_reco)
#    ax.set_title('MC Reco')
#    
#    ax = axs[1,0]
#    c  = ax.plot(ntrue)
#    ax.set_title('PDF True')
#    ax.set_xlabel('$q^2$')
#    
#    ax = axs[1,1]
#    c  = ax.plot(nreco)
#    ax.set_title('PDF Reco')
#    ax.set_xlabel('$q^2$')
#    
#    plt.subplots_adjust(hspace=0.35)
#    fig.savefig('ConvResponseMatrix_q2.pdf')
#    ###############
#    
#    ###############
#    fig, ax = plt.subplots(1)
#    c  = ax.imshow(mij_new, aspect = 'equal', interpolation=None, origin='lower', extent=[edges[0][0], edges[0][-1], edges[1][0], edges[1][-1]], vmin=np.min(mij_new), vmax = np.max(mij_new))
#    ax.set_title(r'2D Reponse Matrix')
#    ax.set_xlabel(r'$q^2_{reco}$')
#    ax.set_ylabel(r'$q^2_{true}$')
#    cbar = fig.colorbar(c, ax=ax)
#    cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#    fig.savefig('ResponseMatrix_q2.pdf')
#    ###############
