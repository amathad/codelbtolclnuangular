#!/bin/python
from root_pandas import read_root
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
import sys

case = sys.argv[1]
bins_str = sys.argv[2]

#define cut
cut='abs(Lb_TRUEID)==5122&&abs(Lc_TRUEID)==4122&&abs(p_TRUEID)==2212&&abs(K_TRUEID)==321&&abs(pi_TRUEID)==211&&abs(mu_TRUEID)==13&&Lb_BKGCAT<60&&'
cut+='Lb_True_Q2_mu>11164.0356&&Lb_True_Q2_mu<11109822.259600001&&Lb_Q2BEST>11164.0356&&Lb_Q2BEST<11109822.259600001&&Lb_True_Costhetal_mu>-1.&&Lb_True_Costhetal_mu<1.&&Lb_COSTHETALBEST>-1.&&Lb_COSTHETALBEST<1.'
bins_reco   = [int(bins_str), int(bins_str)]
bins_true   = [int(bins_str), int(bins_str)]
bins = bins_reco + bins_true
print(bins)
costhl_lim  = (-1., 1.)
q2_lim      = (0.0111640356, 11.109822259600001)
limits      = (q2_lim, costhl_lim, q2_lim, costhl_lim)
print(limits)
#cthl is defined here as angle bw Lc and l in w* rest frame, whereas for us it is (pi - thisangle)
if case == 'q2cthl' or 'q2cthlcthpphp':
    print('Here')
    lget           = ['noexpand:Lb_Q2BEST/1e6', 'noexpand:-Lb_COSTHETALBEST', 'noexpand:Lb_True_Q2_mu/1e6', 'noexpand:-Lb_True_Costhetal_mu']
    ldict          = {'Lb_Q2BEST/1e6':'q2reco', '-Lb_COSTHETALBEST':'cthlreco', 'Lb_True_Q2_mu/1e6':'q2true', '-Lb_True_Costhetal_mu':'cthltrue'}
    df=read_root('/disk/lhcb_data/mferrill/Lcmunu/MC2016/tupleout_background.root', key='tupleout/DecayTree', where=cut, columns=lget) #shape (ndata, 4) 
    df=df.rename(columns=ldict)
    l              = ['q2reco', 'cthlreco', 'q2true', 'cthltrue']
else:
    print('Here 2')
    lget           = ['noexpand:Lb_Q2BEST/1e6', 'noexpand:-Lb_True_Costhetal_mu', 'noexpand:Lb_True_Q2_mu/1e6']
    ldict          = {'Lb_Q2BEST/1e6':'q2reco', '-Lb_True_Costhetal_mu':'cthltrue', 'Lb_True_Q2_mu/1e6':'q2true'}
    df=read_root('/disk/lhcb_data/mferrill/Lcmunu/MC2016/tupleout_background.root', key='tupleout/DecayTree', where=cut, columns=lget) #shape (ndata, 3) 
    df=df.rename(columns=ldict)
    df['cthltrue2'] = df['cthltrue']
    l              = ['q2reco', 'cthltrue', 'q2true', 'cthltrue2']

print(df)
print(df[l].values.shape)
mijkl_norm, edges = np.histogramdd(df[l].values, bins=bins, range=limits) #ALWAYS PASS LIST OF KEYS WHEN CALLING VALUES
print(mijkl_norm.sum())
mijkl_norm = mijkl_norm/mijkl_norm.sum()
print(mijkl_norm.sum())
print(mijkl_norm.shape)
f = lambda a: (a[:-1] + a[1:])/2. 
A, B, C, D = f(edges[0]), f(edges[1]), f(edges[2]), f(edges[3]) 
print('A', A)
print('B', B)
print('C', C)
print('D', D)

nkl = np.einsum('ijkl->kl', mijkl_norm) #should be already normalised, sum over ij (reco)
print(nkl.sum())
mijkl_new = mijkl_norm/nkl #convert to probability that a given true value in bin lie in each of the reco bins
nkl_new = np.einsum('ijkl->kl', mijkl_new); print(nkl_new) #should all be one

import pickle
pickle.dump(mijkl_new, open('/disk/lhcb_data/amathad/diffdensity/responsematrix/'+case+'-'+bins_str+'.p', 'wb'))

if case == 'q2cthl' and bins_str == '20':
    nmc_true, _ = np.histogramdd(df[l[2:]].values, bins=edges[2:], range=(q2_lim, costhl_lim))
    nmc_true = nmc_true/nmc_true.sum()
    print(nmc_true.sum())
    nmc_reco, _ = np.histogramdd(df[l[:2]].values, bins=edges[:2], range=(q2_lim, costhl_lim))
    nmc_reco = nmc_reco/nmc_reco.sum()
    print(nmc_reco.sum())
    
    l_toys = ['q2', 'cthl']
    df_toys = read_root('/disk/lhcb_data/amathad/diffdensity/toys/Sample-SM-Mu-1-4body-partial-'+case+'.root', key='tree', columns = l_toys)
    print(df_toys.shape)
    ntrue, _ = np.histogramdd(df_toys[l_toys].values, bins=edges[2:], range=(q2_lim, costhl_lim))
    ntrue = ntrue/ntrue.sum()
    print(ntrue.sum())
    nreco = np.einsum('ijkl,kl->ij', mijkl_new, ntrue) #should be normalised
    print(nreco.sum())
    
    ##closure test
    #ntrue = nkl
    #nreco = np.einsum('ijkl,kl->ij', mijkl_new, ntrue) #should be normalised

    ################
    zmin_true = min([np.min(nmc_true), np.min(ntrue)])
    zmax_true = max([np.max(nmc_true), np.max(ntrue)])
    zmin_reco = min([np.min(nmc_reco), np.min(nreco)])
    zmax_reco = max([np.max(nmc_reco), np.max(nreco)])
    
    #fig, axs = plt.subplots(2, 2)
    #
    #ax = axs[0,0]
    #c  = ax.imshow(nmc_true, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[2][0], edges[2][-1], edges[3][0], edges[3][-1]], vmin = zmin_true, vmax = zmax_true)
    #ax.set_title('MC True')
    #ax.set_ylabel(r'$cos(\theta_l)$')
    #cbar = fig.colorbar(c, ax=ax)
    #cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #
    #ax = axs[0,1]
    #c  = ax.imshow(nmc_reco, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[0][0], edges[0][-1], edges[1][0], edges[1][-1]], vmin = zmin_reco, vmax = zmax_reco)
    #ax.set_title('MC Reco')
    #cbar = fig.colorbar(c, ax=ax)
    #cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #
    #ax = axs[1,0]
    #c  = ax.imshow(ntrue, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[2][0], edges[2][-1], edges[3][0], edges[3][-1]], vmin = zmin_true, vmax = zmax_true)
    #ax.set_title('PDF True')
    #ax.set_xlabel('$q^2$')
    #ax.set_ylabel(r'$cos(\theta_l)$')
    #cbar = fig.colorbar(c, ax=ax)
    #cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #
    #ax = axs[1,1]
    #c  = ax.imshow(nreco, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[0][0], edges[0][-1], edges[1][0], edges[1][-1]], vmin = zmin_reco, vmax = zmax_reco)
    #ax.set_title('PDF Reco')
    #ax.set_xlabel('$q^2$')
    #cbar = fig.colorbar(c, ax=ax)
    #cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #
    #plt.subplots_adjust(hspace=0.35)
    #fig.savefig('ConvResponseMatrix.pdf')
    ################

    fig, ax = plt.subplots()
    
    c  = ax.imshow(nmc_true, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[2][0], edges[2][-1], edges[3][0], edges[3][-1]], vmin = zmin_true, vmax = zmax_true)
    ax.set_title('MC True')
    ax.set_xlabel('$q^2$')
    ax.set_ylabel(r'$cos(\theta_l)$')
    cbar = fig.colorbar(c, ax=ax)
    cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.subplots_adjust(hspace=0.35)
    fig.savefig('ConvResponseMatrixTrue.pdf')
    
    fig, ax = plt.subplots()
    c  = ax.imshow(nmc_reco, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[0][0], edges[0][-1], edges[1][0], edges[1][-1]], vmin = zmin_reco, vmax = zmax_reco)
    ax.set_title('MC Reco')
    ax.set_xlabel('$q^2$')
    cbar = fig.colorbar(c, ax=ax)
    cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    
    plt.subplots_adjust(hspace=0.35)
    fig.savefig('ConvResponseMatrixReco.pdf')
    ###############
    
    ###############
    fig, ax = plt.subplots()
    
    q = np.einsum('ijkl->ik', mijkl_new)
    c  = ax.imshow(q, aspect = 'auto', interpolation=None, origin='lower', extent=[edges[0][0], edges[0][-1], edges[2][0], edges[2][-1]], vmin=np.min(q), vmax = np.max(q))
    ax.set_title(r'Reponse Matrix')
    ax.set_xlabel(r'$q^2_{reco}$')
    ax.set_ylabel(r'$q^2_{true}$')
    cbar = fig.colorbar(c, ax=ax)
    cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    
    plt.subplots_adjust(hspace=0.5)
    fig.savefig('ResponseMatrixq2.pdf')
    ###############

    ###############
    fig, ax = plt.subplots()
    
    t = np.einsum('ijkl->jl', mijkl_new)
    c  = ax.imshow(t, aspect = 'auto', interpolation='nearest', origin='lower', extent=[edges[1][0], edges[1][-1], edges[3][0], edges[3][-1]], vmin=np.min(q), vmax = np.max(q))
    ax.set_xlabel(r'$cos(\theta_l)_{reco}$')
    ax.set_ylabel(r'$cos(\theta_l)_{true}$')
    cbar = fig.colorbar(c, ax=ax)
    cbar.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    
    plt.subplots_adjust(hspace=0.5)
    fig.savefig('ResponseMatrixcthl.pdf')
    ###############
